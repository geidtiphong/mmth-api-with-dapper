﻿using System.Collections.Generic;

namespace Framework.Helpers {

    public class TreeNode {
        public int Id { get; set; }

        public int ParentId { get; set; }

        public string Name { get; set; }

        public List<TreeNode> Children { get; set; }
        public List<TreeNodeItem> TreeNodeItems { get; set; }
        public TreeNode() {
            Children = new List<TreeNode>();
            TreeNodeItems = new List<TreeNodeItem>();
        }
    }

    public class TreeNodeItem {
        public int ProgramId { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramName { get; set; }
    }

}
