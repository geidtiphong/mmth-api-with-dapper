﻿using System;
using System.Collections.Generic;

namespace Framework.Models
{
	public class ValidationResult
	{
		public Boolean ValidationStatus { get; set; }
		public List<String> ValidationMessages { get; set; }

		public ValidationResult()
		{
			ValidationStatus = true;
			ValidationMessages = new List<String>();
		}
	}
}
