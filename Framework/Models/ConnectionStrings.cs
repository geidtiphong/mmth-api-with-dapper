﻿namespace Framework.Models
{
    public class ConnectionStrings
	{
		public string PrimaryDatabaseConnectionString { get; set; }
        public string Press1_DatabaseConnectionString { get; set; }
        public string Press2_DatabaseConnectionString { get; set; }
        public string Blanking_DatabaseConnectionString { get; set; }
    }
}
