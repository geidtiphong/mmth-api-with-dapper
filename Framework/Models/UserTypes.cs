﻿namespace Framework.Models
{
    public static class UserTypes
    {
		public static int Administrator = 1;
		public static int NonAdministrator = 2;
	}
}
