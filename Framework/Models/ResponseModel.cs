﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace Framework.Models
{
	public class ResponseModel<T>
	{
		public string Token { get; set; }
		public bool ReturnStatus { get; set; }
		public List<string> ReturnMessage { get; set; }
		public Hashtable Errors;
		public int TotalPages;
		public int TotalRows;
		public int PageSize;
		public bool IsAuthenicated;
		public T Entity;

		public ResponseModel()
		{
			ReturnMessage = new List<string>();
			ReturnStatus = true;
			Errors = new Hashtable();
			TotalPages = 0;
            TotalRows = 0;
			PageSize = 0;
			IsAuthenicated = false;
		}
	}

}
