﻿namespace Framework.Models
{
    public class ProgramInfoModel
    {
        public string PROGRAM_CODE { get; set; }
        public string PROGRAM_NAME { get; set; }
        public string PROGRAM_LINK { get; set; }
        public int ORGANIZE_ID { get; set; }
        public string ROLE_CODE { get; set; }
    }
}
