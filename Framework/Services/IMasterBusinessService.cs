﻿using Framework.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Framework.Services
{
    public interface IMasterBusinessService<T>
    {
        Task<ResponseModel<IEnumerable<T>>> GetItemsByCriteria(object whereConditions, int currentPageIndex, string sortExpression, string sortDirection);
        Task<ResponseModel<IEnumerable<T>>> GetItemsByCriteria(string whereClause, int currentPageIndex, string sortExpression, string sortDirection);
        Task<T> SelectItem(T dto);
        Task<T> InsertItem(T dto);
        Task<T> UpdateItem(T dto);
        Task DeleteItem(T dto);
    }
}
