﻿// using Dapper;
// using Microsoft.CSharp.RuntimeBinder;
// using System;
// using System.Collections.Generic;
// using System.Data;
// using System.Diagnostics;
// using System.Linq;
// using System.Reflection;
// using System.Text;
// using System.Threading.Tasks;

// namespace Framework.Services
// {
//     public static partial class RepositoryBase
//     {
//         private static string _getIdentitySql;
//         private static string _encapsulation;

//         static RepositoryBase()
//         {
//             _encapsulation = "[{0}]";
//             _getIdentitySql = string.Format("SELECT CAST(SCOPE_IDENTITY() AS BIGINT) AS [id]");
//         }

//         public static async Task<IEnumerable<T>> GetListBySql<T>(this IDbConnection connection, CommandType commandType, string sql, object parameters = null)
//         {
//             return await connection.QueryAsync<T>(sql, parameters, commandType: commandType);
//         }

//         public static async Task<IEnumerable<T>> GetItems<T>(this IDbConnection connection, CommandType commandType, string sql, object parameters = null)
//         {
//             return await connection.QueryAsync<T>(sql, parameters, commandType: commandType);
//         }

//         public static async Task<IEnumerable<T>> Select<T>(this IDbConnection connection, CommandType commandType, object criteria = null)
//         {
//             var currenttype = typeof(T);
//             var name = GetTableName(currenttype);
//             var properties = ParseProperties(criteria);
//             var sqlPairs = GetSqlPairs(properties.AllNames, " AND ");
//             string sql = string.Format("SELECT * FROM [{0}] WHERE {1}", name, sqlPairs);
//             return await GetItems<T>(connection, CommandType.Text, sql, properties.AllPairs);
//         }

//         public static async Task<int> Execute(this IDbConnection connection, CommandType commandType, string sql, object parameters = null)
//         {
//             return await connection.ExecuteAsync(sql, parameters, commandType: commandType);
//         }

//         /// <summary>
//         /// <para>By default queries the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>By default filters on the Id column</para>
//         /// <para>-Id column name can be overridden by adding an attribute on your primary key property [Key]</para>
//         /// <para>Returns a single entity by a single id from table T</para>
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="connection"></param>
//         /// <param name="entity"></param>
//         /// <returns>Returns a single entity by a single id from table T.</returns>
//         public async static Task<T> Get<T>(this IDbConnection connection, int id) {
//             var currenttype = typeof(T);
//             var idProps = GetIdProperties(currenttype).ToList();

//             if (!idProps.Any())
//                 throw new ArgumentException("Get<T> only supports an entity with a [Key] or Id property");
//             if (idProps.Count() > 1)
//                 throw new ArgumentException("Get<T> only supports an entity with a single [Key] or Id property");

//             var onlyKey = idProps.First();
//             var name = GetTableName(currenttype);
//             var sb = new StringBuilder();
//             sb.Append("Select ");
//             //create a new empty instance of the type to get the base properties
//             BuildSelect(sb, GetScaffoldableProperties((T)Activator.CreateInstance(typeof(T))).ToArray());
//             sb.AppendFormat(" from {0}", name);
//             sb.Append(" where " + GetColumnName(onlyKey) + " = @Id");

//             var dynParms = new DynamicParameters();
//             dynParms.Add("@id", id);

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("Get<{0}>: {1} with Id: {2}", currenttype, sb, id));

//             IEnumerable<T> entities = await connection.QueryAsync<T>(sb.ToString(), dynParms); //.FirstOrDefault();

//             return entities.AsList().FirstOrDefault();
//         }
//         public async static Task<T> Get<T>(this IDbConnection connection, object entity)
//         {
//             var currenttype = typeof(T);
//             var idProps = GetIdProperties(currenttype).ToList();

//             if (!idProps.Any())
//                 throw new ArgumentException("Get<T> only supports an entity with a [Key] or Id property");
//             if (idProps.Count() > 1)
//                 throw new ArgumentException("Get<T> only supports an entity with a single [Key] or Id property");

//             var onlyKey = idProps.First();
//             var name = GetTableName(currenttype);
//             var sb = new StringBuilder();
//             sb.Append("Select ");
//             //create a new empty instance of the type to get the base properties
//             BuildSelect(sb, GetScaffoldableProperties((T)Activator.CreateInstance(typeof(T))).ToArray());
//             sb.AppendFormat(" from {0}", name);

//             sb.Append(" where ");
//             BuildWhere(sb, idProps, entity);

//             var dynParms = new DynamicParameters();
//             var property = entity.GetType().GetProperties().ToArray().First(it=>it.Name.Equals(onlyKey.Name));
//             dynParms.Add(string.Format("@{0}", onlyKey.Name), property.GetValue(entity, null));

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("Get<{0}>: {1} with Id: {2}", currenttype, sb, entity));

//             IEnumerable<T> entities = await connection.QueryAsync<T>(sb.ToString(), dynParms);

//             return entities.AsList().FirstOrDefault();
//         }

//         /// <summary>
//         /// <para>By default queries the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>whereConditions is an anonymous type to filter the results ex: new {Category = 1, SubCategory=2}</para>
//         /// <para>Returns a list of entities that match where conditions</para>
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="connection"></param>
//         /// <param name="whereConditions"></param>
//         /// <returns>Gets a list of entities with optional exact match where conditions</returns>
//         public async static Task<IEnumerable<T>> GetList<T>(this IDbConnection connection, object whereConditions)
//         {
//             var currenttype = typeof(T);
//             var idProps = GetIdProperties(currenttype).ToList();
//             if (!idProps.Any())
//                 throw new ArgumentException("Entity must have at least one [Key] property");

//             var name = GetTableName(currenttype);

//             var sb = new StringBuilder();
//             var whereprops = GetAllProperties(whereConditions).ToArray();
//             sb.Append("Select ");
//             //create a new empty instance of the type to get the base properties
//             BuildSelect(sb, GetScaffoldableProperties((T)Activator.CreateInstance(typeof(T))).ToArray());
//             sb.AppendFormat(" from {0}", name);

//             if (whereprops.Any())
//             {
//                 sb.Append(" where ");
//                 BuildWhere(sb, whereprops, (T)Activator.CreateInstance(typeof(T)));
//             }

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("GetList<{0}>: {1}", currenttype, sb));

//             return await connection.QueryAsync<T>(sb.ToString(), whereConditions);
//         }

//         /// <summary>
//         /// <para>By default queries the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>conditions is an SQL where clause and/or order by clause ex: "where name='bob'"</para>
//         /// <para>Returns a list of entities that match where conditions</para>
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="connection"></param>
//         /// <param name="conditions"></param>
//         /// <returns>Gets a list of entities with optional SQL where conditions</returns>
//         public async static Task<IEnumerable<T>> GetList<T>(this IDbConnection connection, string conditions)
//         {
//             var currenttype = typeof(T);
//             var idProps = GetIdProperties(currenttype).ToList();
//             if (!idProps.Any())
//                 throw new ArgumentException("Entity must have at least one [Key] property");

//             var name = GetTableName(currenttype);

//             var sb = new StringBuilder();
//             var whereprops = GetAllProperties(conditions).ToArray();
//             sb.Append("Select ");
//             //create a new empty instance of the type to get the base properties
//             BuildSelect(sb, GetScaffoldableProperties((T)Activator.CreateInstance(typeof(T))).ToArray());
//             sb.AppendFormat(" from {0}", name);

//             sb.Append(" " + conditions);

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("GetList<{0}>: {1}", currenttype, sb));

//             return await connection.QueryAsync<T>(sb.ToString());
//         }


//         /// <summary>
//         /// <para>By default queries the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>Returns a list of all entities</para>
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="connection"></param>
//         /// <returns>Gets a list of all entities</returns>
//         public static Task<IEnumerable<T>> GetList<T>(this IDbConnection connection)
//         {
//             return connection.GetList<T>(new { });
//         }

//         /// <summary>
//         /// <para>Inserts a row into the database</para>
//         /// <para>By default inserts into the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>Insert filters out Id column and any columns with the [Key] attribute</para>
//         /// <para>Properties marked with attribute [Editable(false)] and complex types are ignored</para>
//         /// <para>Supports transaction and command timeout</para>
//         /// <para>Returns the ID (primary key) of the newly inserted record if it is identity using the int? type, otherwise null</para>
//         /// </summary>
//         /// <param name="connection"></param>
//         /// <param name="entityToInsert"></param>
//         /// <param name="transaction"></param>
//         /// <param name="commandTimeout"></param>
//         /// <returns>The ID (primary key) of the newly inserted record if it is identity using the int? type, otherwise null</returns>
//         public static async Task<int?> Insert(this IDbConnection connection, object entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
//         {
//             //return Insert<int?>(connection, entityToInsert, transaction, commandTimeout);
//             return await Insert<int?>(connection, entityToInsert, transaction, commandTimeout);
//         }



//         /// <summary>
//         /// </summary>
//         /// <param name="connection"></param>
//         /// <param name="entityToInsert"></param>
//         /// <param name="transaction"></param>
//         /// <param name="commandTimeout"></param>
//         /// <returns></returns>
//         public static int InsertNoPkConstraint(this IDbConnection connection,
//             object entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
//         {
//             var sb = InsertBuilder(entityToInsert);

//             return connection.Execute(sb.ToString(), entityToInsert, transaction, commandTimeout);
//         }



//         /// <summary>
//         /// <para>Inserts a row into the database</para>
//         /// <para>By default inserts into the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>Insert filters out Id column and any columns with the [Key] attribute</para>
//         /// <para>Properties marked with attribute [Editable(false)] and complex types are ignored</para>
//         /// <para>Supports transaction and command timeout</para>
//         /// <para>Returns the ID (primary key) of the newly inserted record if it is identity using the defined type, otherwise null</para>
//         /// </summary>
//         /// <param name="connection"></param>
//         /// <param name="entityToInsert"></param>
//         /// <param name="transaction"></param>
//         /// <param name="commandTimeout"></param>
//         /// <returns>The ID (primary key) of the newly inserted record if it is identity using the defined type, otherwise null</returns>
//         public static async Task<TKey> Insert<TKey>(this IDbConnection connection, object entityToInsert, IDbTransaction transaction = null, int? commandTimeout = null)
//         {
//             var idProps = GetIdProperties(entityToInsert).ToList();
//             if (!idProps.Any())
//                 throw new ArgumentException("Insert<T> only supports an entity with a [Key] or Id property");
//             if (idProps.Count() > 1)
//                 throw new ArgumentException("Insert<T> only supports an entity with a single [Key] or Id property");

//             var baseType = typeof(TKey);
//             var underlyingType = Nullable.GetUnderlyingType(baseType);
//             var keytype = underlyingType ?? baseType;
//             if (keytype != typeof(int) && keytype != typeof(uint) && keytype != typeof(long) && keytype != typeof(ulong) && keytype != typeof(short) && keytype != typeof(ushort) && keytype != typeof(Guid))
//             {
//                 throw new Exception("Invalid return type");
//             }
//             if (keytype == typeof(Guid))
//             {
//                 var guidvalue = (Guid)idProps.First().GetValue(entityToInsert, null);
//                 if (guidvalue == Guid.Empty)
//                 {
//                     var newguid = SequentialGuid();
//                     idProps.First().SetValue(entityToInsert, newguid, null);
//                 }
//             }

//             var sb = InsertBuilder(entityToInsert);

//             if (keytype == typeof(Guid))
//             {
//                 sb.Append(";select '" + idProps.First().GetValue(entityToInsert, null) + "' as id");
//             }
//             else
//             {
//                 sb.Append(";" + _getIdentitySql);
//             }

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("Insert: {0}", sb));

//             //var r = connection.Query(sb.ToString(), entityToInsert, transaction, true, commandTimeout);
//             var r = await connection.QueryAsync(sb.ToString(), entityToInsert);//, true, commandTimeout);

//             if (keytype == typeof(Guid))
//             {
//                 return (TKey)idProps.First().GetValue(entityToInsert, null);
//             }
//             return (TKey)r.First().id;

//            // return (TKey)vallues;
//         }

//         private static StringBuilder InsertBuilder(object entityToInsert)
//         {
//             var name = GetTableName(entityToInsert);
//             var sb = new StringBuilder();

//             sb.AppendFormat("insert into {0}", name);
//             sb.Append(" (");
//             BuildInsertParameters(entityToInsert, sb);
//             sb.Append(") ");

//             sb.Append("values");
//             sb.Append(" (");
//             BuildInsertValues(entityToInsert, sb);
//             sb.Append(")");

//             return sb;
//         }


//         /// <summary>
//         /// <para>Updates a record or records in the database</para>
//         /// <para>By default updates records in the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>Updates records where the Id property and properties with the [Key] attribute match those in the database.</para>
//         /// <para>Properties marked with attribute [Editable(false)] and complex types are ignored</para>
//         /// <para>Supports transaction and command timeout</para>
//         /// <para>Returns number of rows effected</para>
//         /// </summary>
//         /// <param name="connection"></param>
//         /// <param name="entityToUpdate"></param>
//         /// <param name="transaction"></param>
//         /// <param name="commandTimeout"></param>
//         /// <returns>The number of effected records</returns>
//         public async static Task<int> Update(this IDbConnection connection, object entityToUpdate, IDbTransaction transaction = null, int? commandTimeout = null)
//         {
//             var idProps = GetIdProperties(entityToUpdate).ToList();

//             if (!idProps.Any())
//                 throw new ArgumentException("Entity must have at least one [Key] or Id property");

//             var name = GetTableName(entityToUpdate);

//             var sb = new StringBuilder();
//             sb.AppendFormat("update {0}", name);

//             sb.AppendFormat(" set ");
//             BuildUpdateSet(entityToUpdate, sb);
//             sb.Append(" where ");
//             BuildWhere(sb, idProps, entityToUpdate);

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("Update: {0}", sb));

//             //return connection.Execute(sb.ToString(), entityToUpdate, transaction, commandTimeout);
//             return await connection.ExecuteAsync(sb.ToString(), entityToUpdate, transaction, commandTimeout);
//         }

//         /// <summary>
//         /// <para>Deletes a record or records in the database that match the object passed in</para>
//         /// <para>-By default deletes records in the table matching the class name</para>
//         /// <para>Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>Supports transaction and command timeout</para>
//         /// <para>Returns the number of records effected</para>
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="connection"></param>
//         /// <param name="entityToDelete"></param>
//         /// <param name="transaction"></param>
//         /// <param name="commandTimeout"></param>
//         /// <returns>The number of records effected</returns>
//         public async static Task<int> Delete<T>(this IDbConnection connection, T entityToDelete, IDbTransaction transaction = null, int? commandTimeout = null)
//         {
//             var idProps = GetIdProperties(entityToDelete).ToList();


//             if (!idProps.Any())
//                 throw new ArgumentException("Entity must have at least one [Key] or Id property");

//             var name = GetTableName(entityToDelete);

//             var sb = new StringBuilder();
//             sb.AppendFormat("delete from {0}", name);

//             sb.Append(" where ");
//             BuildWhere(sb, idProps, entityToDelete);

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("Delete: {0}", sb));

//             return await connection.ExecuteAsync(sb.ToString(), entityToDelete, transaction, commandTimeout);
//         }

//         /// <summary>
//         /// <para>Deletes a record or records in the database by ID</para>
//         /// <para>By default deletes records in the table matching the class name</para>
//         /// <para>-Table name can be overridden by adding an attribute on your class [Table("YourTableName")]</para>
//         /// <para>Deletes records where the Id property and properties with the [Key] attribute match those in the database</para>
//         /// <para>The number of records effected</para>
//         /// <para>Supports transaction and command timeout</para>
//         /// </summary>
//         /// <typeparam name="T"></typeparam>
//         /// <param name="connection"></param>
//         /// <param name="id"></param>
//         /// <param name="transaction"></param>
//         /// <param name="commandTimeout"></param>
//         /// <returns>The number of records effected</returns>
//         public async static Task<int> Delete<T>(this IDbConnection connection, object id, IDbTransaction transaction = null, int? commandTimeout = null)
//         {
//             var currenttype = typeof(T);
//             var idProps = GetIdProperties(currenttype).ToList();


//             if (!idProps.Any())
//                 throw new ArgumentException("Delete<T> only supports an entity with a [Key] or Id property");
//             if (idProps.Count() > 1)
//                 throw new ArgumentException("Delete<T> only supports an entity with a single [Key] or Id property");

//             var onlyKey = idProps.First();
//             var name = GetTableName(currenttype);

//             var sb = new StringBuilder();
//             sb.AppendFormat("Delete from {0}", name);
//             sb.Append(" where " + GetColumnName(onlyKey) + " = @Id");

//             var dynParms = new DynamicParameters();
//             dynParms.Add("@id", id);

//             if (Debugger.IsAttached)
//                 Trace.WriteLine(String.Format("Delete<{0}> {1}", currenttype, sb));

//             //return connection.Execute(sb.ToString(), dynParms, transaction, commandTimeout);
//             return await connection.ExecuteAsync(sb.ToString(), dynParms);
//         }

//         //build update statement based on list on an entity
//         private static void BuildUpdateSet(object entityToUpdate, StringBuilder sb)
//         {
//             var nonIdProps = GetUpdateableProperties(entityToUpdate).ToArray();

//             for (var i = 0; i < nonIdProps.Length; i++)
//             {
//                 var property = nonIdProps[i];

//                 sb.AppendFormat("{0} = @{1}", GetColumnName(property), property.Name);
//                 if (i < nonIdProps.Length - 1)
//                     sb.AppendFormat(", ");
//             }
//         }

//         //build select clause based on list of properties
//         private static void BuildSelect(StringBuilder sb, IEnumerable<PropertyInfo> props)
//         {
//             var propertyInfos = props as IList<PropertyInfo> ?? props.ToList();
//             for (var i = 0; i < propertyInfos.Count(); i++)
//             {
//                 sb.Append(GetColumnName(propertyInfos.ElementAt(i)));
//                 //if there is a custom column name add an "as customcolumnname" to the item so it maps properly
//                 if (propertyInfos.ElementAt(i).GetCustomAttributes(true).SingleOrDefault(attr => attr.GetType().Name == "ColumnAttribute") != null)
//                     sb.Append(" as " + propertyInfos.ElementAt(i).Name + " ");
//                 if (i < propertyInfos.Count() - 1)
//                     sb.Append(",");
//             }
//         }

//         //build where clause based on list of properties
//         private static void BuildWhere(StringBuilder sb, IEnumerable<PropertyInfo> idProps, object sourceEntity)
//         {
//             var propertyInfos = idProps.ToArray();
//             for (var i = 0; i < propertyInfos.Count(); i++)
//             {
//                 //match up generic properties to source entity properties to allow fetching of the column attribute
//                 //the anonymous object used for search doesn't have the custom attributes attached to them so this allows us to build the correct where clause
//                 //by converting the model type to the database column name via the column attribute
//                 var propertyToUse = propertyInfos.ElementAt(i);
//                 var sourceProperties = GetScaffoldableProperties(sourceEntity).ToArray();
//                 for (var x = 0; x < sourceProperties.Count(); x++)
//                 {
//                     if (sourceProperties.ElementAt(x).Name == propertyInfos.ElementAt(i).Name)
//                     {
//                         propertyToUse = sourceProperties.ElementAt(x);
//                     }
//                 }

//                 sb.AppendFormat("{0} = @{1}", GetColumnName(propertyToUse), propertyInfos.ElementAt(i).Name);
//                 if (i < propertyInfos.Count() - 1)
//                     sb.AppendFormat(" and ");
//             }
//         }


//         //build insert values which include all properties in the class that are not marked with the Editable(false) attribute,
//         //are not marked with the [Key] attribute, and are not named Id
//         private static void BuildInsertValues(object entityToInsert, StringBuilder sb)
//         {
//             var props = GetScaffoldableProperties(entityToInsert).ToArray();
//             for (var i = 0; i < props.Count(); i++)
//             {
//                 var property = props.ElementAt(i);

//                 // clabnet 21/6/2015
//                 // for adding entities with Primary Key named as "id" but No Sequence,
//                 // with Key and Required attributes specified.  
//                 if (property.PropertyType != typeof(Guid)
//                 && property.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "KeyAttribute")
//                 && !property.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "RequiredAttribute"))
//                     continue;

//                 if (property.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "ReadOnlyAttribute" && IsReadOnly(property))) continue;

//                 if (property.Name == "Id") continue;
//                 sb.AppendFormat("@{0}", property.Name);
//                 if (i < props.Count() - 1)
//                     sb.Append(", ");
//             }
//             if (sb.ToString().EndsWith(", "))
//                 sb.Remove(sb.Length - 2, 2);

//         }

//         //build insert parameters which include all properties in the class that are not marked with the Editable(false) attribute,
//         //are not marked with the [Key] attribute, and are not named Id
//         private static void BuildInsertParameters(object entityToInsert, StringBuilder sb)
//         {
//             var props = GetScaffoldableProperties(entityToInsert).ToArray();

//             for (var i = 0; i < props.Count(); i++)
//             {
//                 var property = props.ElementAt(i);

//                 // clabnet 21/6/2015
//                 // for adding entities with Primary Key named as "id" but No Sequence,
//                 // with Key and Required attributes specified.  
//                 if (property.PropertyType != typeof(Guid)
//                 && property.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "KeyAttribute")
//                 && !property.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "RequiredAttribute"))
//                     continue;

//                 if (property.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "ReadOnlyAttribute" && IsReadOnly(property))) continue;
//                 if (property.Name == "Id") continue;
//                 sb.Append(GetColumnName(property));
//                 if (i < props.Count() - 1)
//                     sb.Append(", ");
//             }
//             if (sb.ToString().EndsWith(", "))
//                 sb.Remove(sb.Length - 2, 2);
//         }

//         //Get all properties in an entity
//         private static IEnumerable<PropertyInfo> GetAllProperties(object entity)
//         {
//             if (entity == null) entity = new { };
//             return entity.GetType().GetProperties();
//         }

//         //Get all properties that are not decorated with the Editable(false) attribute
//         private static IEnumerable<PropertyInfo> GetScaffoldableProperties(object entity)
//         {
//             var props = entity.GetType().GetProperties().Where(p => p.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "EditableAttribute" && !IsEditable(p)) == false);
//             return props.Where(p => p.PropertyType.IsSimpleType() || IsEditable(p));
//         }

//         //Determine if the Attribute has an AllowEdit key and return its boolean state
//         //fake the funk and try to mimick EditableAttribute in System.ComponentModel.DataAnnotations 
//         //This allows use of the DataAnnotations property in the model and have the SimpleCRUD engine just figure it out without a reference
//         private static bool IsEditable(PropertyInfo pi)
//         {
//             var attributes = pi.GetCustomAttributes(false);
//             if (attributes.Length > 0)
//             {
//                 dynamic write = attributes.FirstOrDefault(x => x.GetType().Name == "EditableAttribute");
//                 if (write != null)
//                 {
//                     return write.AllowEdit;
//                 }
//             }
//             return false;
//         }


//         //Determine if the Attribute has an IsReadOnly key and return its boolean state
//         //fake the funk and try to mimick ReadOnlyAttribute in System.ComponentModel 
//         //This allows use of the DataAnnotations property in the model and have the SimpleCRUD engine just figure it out without a reference
//         private static bool IsReadOnly(PropertyInfo pi)
//         {
//             var attributes = pi.GetCustomAttributes(false);
//             if (attributes.Length > 0)
//             {
//                 dynamic write = attributes.FirstOrDefault(x => x.GetType().Name == "ReadOnlyAttribute");
//                 if (write != null)
//                 {
//                     return write.IsReadOnly;
//                 }
//             }
//             return false;
//         }

//         //Get all properties that are NOT named Id, DO NOT have the Key attribute, and are not marked ReadOnly
//         private static IEnumerable<PropertyInfo> GetUpdateableProperties(object entity)
//         {
//             var updateableProperties = GetScaffoldableProperties(entity);
//             //remove ones with ID
//             updateableProperties = updateableProperties.Where(p => p.Name != "Id");
//             //remove ones with key attribute
//             updateableProperties = updateableProperties.Where(p => p.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "KeyAttribute") == false);
//             //remove ones that are readonly
//             updateableProperties = updateableProperties.Where(p => p.GetCustomAttributes(true).Any(attr => (attr.GetType().Name == "ReadOnlyAttribute") && IsReadOnly(p)) == false);

//             return updateableProperties;
//         }

//         //Get all properties that are named Id or have the Key attribute
//         //For Inserts and updates we have a whole entity so this method is used
//         private static IEnumerable<PropertyInfo> GetIdProperties(object entity)
//         {
//             var type = entity.GetType();
//             return GetIdProperties(type);
//         }

//         //Get all properties that are named Id or have the Key attribute
//         //For Get(id) and Delete(id) we don't have an entity, just the type so this method is used
//         private static IEnumerable<PropertyInfo> GetIdProperties(Type type)
//         {
//             var tp = type.GetProperties().Where(p => p.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "KeyAttribute")).ToList();
//             return tp.Any() ? tp : type.GetProperties().Where(p => p.Name == "Id");
//         }


//         //Gets the table name for this entity
//         //For Inserts and updates we have a whole entity so this method is used
//         //Uses class name by default and overrides if the class has a Table attribute
//         private static string GetTableName(object entity)
//         {
//             var type = entity.GetType();
//             return GetTableName(type);
//         }

//         //Gets the table name for this type
//         //For Get(id) and Delete(id) we don't have an entity, just the type so this method is used
//         //Use dynamic type to be able to handle both our Table-attribute and the DataAnnotation
//         //Uses class name by default and overrides if the class has a Table attribute
//         private static string GetTableName(Type type)
//         {
//             //var tableName = String.Format("[{0}]", type.Name);
//             var tableName = Encapsulate(type.Name);

//             var tableattr = type.GetCustomAttributes(true).SingleOrDefault(attr => attr.GetType().Name == "TableAttribute") as dynamic;
//             if (tableattr != null)
//             {
//                 //tableName = String.Format("[{0}]", tableattr.Name);
//                 tableName = Encapsulate(tableattr.Name);
//                 try
//                 {
//                     if (!String.IsNullOrEmpty(tableattr.Schema))
//                     {
//                         //tableName = String.Format("[{0}].[{1}]", tableattr.Schema, tableattr.Name);
//                         string schemaName = Encapsulate(tableattr.Schema);
//                         tableName = String.Format("{0}.{1}", schemaName, tableName);
//                     }
//                 }
//                 catch (RuntimeBinderException)
//                 {
//                     //Schema doesn't exist on this attribute.
//                 }
//             }

//             return tableName;
//         }

//         private static string GetColumnName(PropertyInfo propertyInfo)
//         {
//             var columnName = Encapsulate(propertyInfo.Name);

//             var columnattr = propertyInfo.GetCustomAttributes(true).SingleOrDefault(attr => attr.GetType().Name == "ColumnAttribute") as dynamic;
//             if (columnattr != null)
//             {
//                 columnName = Encapsulate(columnattr.Name);
//                 Trace.WriteLine(String.Format("Column name for type overridden from {0} to {1}", propertyInfo.Name, columnName));
//             }
//             return columnName;
//         }

//         private static string Encapsulate(string databaseword)
//         {
//             return string.Format(_encapsulation, databaseword);
//         }
//         /// <summary>
//         /// Generates a guid based on the current date/time
//         /// http://stackoverflow.com/questions/1752004/sequential-guid-generator-c-sharp
//         /// </summary>
//         /// <returns></returns>
//         public static Guid SequentialGuid()
//         {
//             var tempGuid = Guid.NewGuid();
//             var bytes = tempGuid.ToByteArray();
//             var time = DateTime.Now;
//             bytes[3] = (byte)time.Year;
//             bytes[2] = (byte)time.Month;
//             bytes[1] = (byte)time.Day;
//             bytes[0] = (byte)time.Hour;
//             bytes[5] = (byte)time.Minute;
//             bytes[4] = (byte)time.Second;
//             return new Guid(bytes);
//         }

//         #region Reflection support

//         /// <summary>
//         /// Retrieves a Dictionary with name and value 
//         /// for all object properties matching the given criteria.
//         /// </summary>
//         private static PropertyContainer ParseProperties<T>(T obj)
//         {
//             var propertyContainer = new PropertyContainer();

//             var typeName = typeof(T).Name;
//             var validKeyNames = new[] { "Id",
//             string.Format("{0}Id", typeName), string.Format("{0}_Id", typeName) };

//             var properties = typeof(T).GetProperties();
//             foreach (var property in properties)
//             {
//                 // Skip reference types (but still include string!)
//                 if (property.PropertyType.IsClass && property.PropertyType != typeof(string))
//                     continue;

//                 // Skip methods without a public setter
//                 if (property.GetSetMethod() == null)
//                     continue;

//                 // Skip methods specifically ignored
//                 if (property.IsDefined(typeof(DapperIgnore), false))
//                     continue;

//                 var name = property.Name;
//                 var value = typeof(T).GetProperty(property.Name).GetValue(obj, null);

//                 if (property.IsDefined(typeof(DapperKey), false) || validKeyNames.Contains(name))
//                 {
//                     propertyContainer.AddId(name, value);
//                 }
//                 else
//                 {
//                     propertyContainer.AddValue(name, value);
//                 }
//             }

//             return propertyContainer;
//         }

//         /// <summary>
//         /// Create a commaseparated list of value pairs on 
//         /// the form: "key1=@value1, key2=@value2, ..."
//         /// </summary>
//         private static string GetSqlPairs
//         (IEnumerable<string> keys, string separator = ", ")
//         {
//             var pairs = keys.Select(key => string.Format("{0}=@{0}", key)).ToList();
//             return string.Join(separator, pairs);
//         }

//         private static void SetId<T>(T obj, int id, IDictionary<string, object> propertyPairs)
//         {
//             if (propertyPairs.Count == 1)
//             {
//                 var propertyName = propertyPairs.Keys.First();
//                 var propertyInfo = obj.GetType().GetProperty(propertyName);
//                 if (propertyInfo.PropertyType == typeof(int))
//                 {
//                     propertyInfo.SetValue(obj, id, null);
//                 }
//             }
//         }

//         #endregion

//         private class PropertyContainer
//         {
//             private readonly Dictionary<string, object> _ids;
//             private readonly Dictionary<string, object> _values;

//             #region Properties

//             internal IEnumerable<string> IdNames
//             {
//                 get { return _ids.Keys; }
//             }

//             internal IEnumerable<string> ValueNames
//             {
//                 get { return _values.Keys; }
//             }

//             internal IEnumerable<string> AllNames
//             {
//                 get { return _ids.Keys.Union(_values.Keys); }
//             }

//             internal IDictionary<string, object> IdPairs
//             {
//                 get { return _ids; }
//             }

//             internal IDictionary<string, object> ValuePairs
//             {
//                 get { return _values; }
//             }

//             internal IEnumerable<KeyValuePair<string, object>> AllPairs
//             {
//                 get { return _ids.Concat(_values); }
//             }

//             #endregion

//             #region Constructor

//             internal PropertyContainer()
//             {
//                 _ids = new Dictionary<string, object>();
//                 _values = new Dictionary<string, object>();
//             }

//             #endregion

//             #region Methods

//             internal void AddId(string name, object value)
//             {
//                 _ids.Add(name, value);
//             }

//             internal void AddValue(string name, object value)
//             {
//                 _values.Add(name, value);
//             }

//             #endregion
//         }
//     }
// }

// internal static class TypeExtension
// {
//     //You can't insert or update complex types. Lets filter them out.
//     public static bool IsSimpleType(this Type type)
//     {
//         var underlyingType = Nullable.GetUnderlyingType(type);
//         type = underlyingType ?? type;
//         var simpleTypes = new List<Type>
//                                {
//                                    typeof(byte),
//                                    typeof(sbyte),
//                                    typeof(short),
//                                    typeof(ushort),
//                                    typeof(int),
//                                    typeof(uint),
//                                    typeof(long),
//                                    typeof(ulong),
//                                    typeof(float),
//                                    typeof(double),
//                                    typeof(decimal),
//                                    typeof(bool),
//                                    typeof(string),
//                                    typeof(char),
//                                    typeof(Guid),
//                                    typeof(DateTime),
//                                    typeof(DateTimeOffset),
//                                    typeof(byte[])
//                                };
//         return simpleTypes.Contains(type) || type.IsEnum;
//     }
// }
