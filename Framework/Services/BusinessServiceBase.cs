﻿using System.Data;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Framework.Models;
using Microsoft.AspNetCore.Http;

namespace Framework.Services {
    [AttributeUsage(AttributeTargets.Property)]
    public class DapperKey : Attribute {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DapperIgnore : Attribute {
    }

    public abstract class BusinessServiceBase {
        protected IDbConnection con;
        private string _version;

        protected string _connectionString;

        #region Constructor

        protected BusinessServiceBase(IOptions<ConnectionStrings> config) {
            //_connectionString = config.Value.PrimaryDatabaseConnectionString;
            HttpContextAccessor _httpContextAccessor = new HttpContextAccessor();

            string organize_id = "";
            if (_httpContextAccessor.HttpContext.Request.Headers["ORGANIZE_ID"].ToString() != null)
                organize_id = _httpContextAccessor.HttpContext.Request.Headers["ORGANIZE_ID"].ToString();

            if (organize_id == "7")
                _connectionString = config.Value.Press1_DatabaseConnectionString;
            else if (organize_id == "8")
                _connectionString = config.Value.Press2_DatabaseConnectionString;
            else if (organize_id == "10")
                _connectionString = config.Value.Blanking_DatabaseConnectionString;
            else
                _connectionString = config.Value.PrimaryDatabaseConnectionString;
                //_connectionString = config.Value._3C_DatabaseConnectionString;

            _version = "1.0.0";
        }

        #endregion

        public string Version() {
            return _version;
        }

        #region Standard Dapper functionality

        protected async Task<IEnumerable<T>> GetItems<T>(CommandType commandType, string sql, object parameters = null) {
            using (var connection = GetOpenConnection()) {
                return await connection.QueryAsync<T>(sql, parameters, commandType: commandType);
            }
        }

        protected async Task<int> Execute(CommandType commandType, string sql, object parameters = null) {
            using (var connection = GetOpenConnection()) {
                return await connection.ExecuteAsync(sql, parameters, commandType: commandType);
            }
        }

        protected SqlConnection GetOpenConnection() {
            var connection = new SqlConnection(this._connectionString);
            connection.Open();
            return connection;
        }

        #endregion

        public async Task<ResponseModel<ProgramInfoModel>> GetProgramInfo(ProgramInfoModel programInfoModel) {
            ResponseModel<ProgramInfoModel> returnResponse = new ResponseModel<ProgramInfoModel>();
            ProgramInfoModel result = null;

            try {
                using (SqlConnection connection = GetOpenConnection()) {
                    var query = "GetProgramInfo";

                    var dynamicParameters = new DynamicParameters();
                    dynamicParameters.Add("@ORGANIZE_ID", programInfoModel.ORGANIZE_ID);
                    dynamicParameters.Add("@ROLE_CODE", programInfoModel.ROLE_CODE);
                    dynamicParameters.Add("@PROGRAM_CODE", programInfoModel.PROGRAM_CODE);

                    dynamic entity = await SqlMapper.QuerySingleOrDefaultAsync<dynamic>(connection, query, dynamicParameters, commandType: System.Data.CommandType.StoredProcedure);
                    if (entity != null) {
                        result = new ProgramInfoModel() { ORGANIZE_ID = entity.ORGANIZE_ID, ROLE_CODE = entity.ROLE_CODE, PROGRAM_CODE = entity.PROGRAM_CODE, PROGRAM_NAME = entity.PROGRAM_NAME, PROGRAM_LINK = entity.PROGRAM_LINK };
                    } else {
                        throw new Exception("Invalid authorization! [Organize, Role and Program]");
                    }

                    returnResponse.Entity = result;
                    returnResponse.ReturnStatus = true;
                }
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

    }
}
