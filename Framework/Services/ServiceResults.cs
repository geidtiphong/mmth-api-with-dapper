﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Services
{
    public class ServiceResults<T>
    {
        public IEnumerable<T> Entities { get; set; }
        public T Entity { get; set; }
        public int PageSize { get; set; }
        public int TotalRows { get; set; }
    }
}
