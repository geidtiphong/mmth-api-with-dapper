﻿// using Dapper;
// using Framework.Models;
// using Microsoft.CSharp.RuntimeBinder;
// using Microsoft.Extensions.Options;
// using System;
// using System.Collections.Generic;
// using System.Data.SqlClient;
// using System.Linq;
// using System.Linq.Expressions;
// using System.Reflection;
// using System.Text;
// using System.Threading.Tasks;
// using Framework.Utilities;
// using Microsoft.AspNetCore.Http;

// namespace Framework.Services {
//     public abstract class MasterBusinessServiceBase<T> {
//         protected string _connectionString;
//         private static string _encapsulation;
//         protected int pageSize;

//         #region Constructor

//         protected MasterBusinessServiceBase(IOptions<ConnectionStrings> config) {
//             //_connectionString = config.Value.PrimaryDatabaseConnectionString;

//             HttpContextAccessor _httpContextAccessor = new HttpContextAccessor();

//             string organize_id = "";
//             if (_httpContextAccessor.HttpContext.Request.Headers["ORGANIZE_ID"].ToString() != null)
//                 organize_id = _httpContextAccessor.HttpContext.Request.Headers["ORGANIZE_ID"].ToString();

//             if (organize_id == "7")
//                 _connectionString = config.Value.Press1_DatabaseConnectionString;
//             else if (organize_id == "8")
//                 _connectionString = config.Value.Press2_DatabaseConnectionString;
//             else if (organize_id == "10")
//                 _connectionString = config.Value.Blanking_DatabaseConnectionString;
//             else
//                 _connectionString = config.Value.PrimaryDatabaseConnectionString;
//             //_connectionString = config.Value._3C_DatabaseConnectionString;

//             _encapsulation = "[{0}]";
//             pageSize = 20;
//         }

//         #endregion

//         protected SqlConnection GetConnection() {
//             var conn = new SqlConnection(_connectionString);
//             conn.Open();

//             return conn;
//         }

//         #region Reflection

//         protected string SqlOrderByWithPaging(string orderByColumnName) {
//             return string.Format("ORDER BY [{0}] OFFSET @PageSize * (@CurrentPageNumber - 1) ROWS FETCH NEXT @PageSize ROWS ONLY OPTION (RECOMPILE)", orderByColumnName);
//         }

//         protected string SqlCountRows() {
//             var currenttype = typeof(T);
//             var tableName = GetTableName(currenttype);

//             return string.Format("SELECT COUNT(*) AS CNT FROM {0}", tableName);
//         }

//         protected string SqlWhereLikeClause(object criterias) {
//             StringBuilder whereClause = new StringBuilder();
//             whereClause.Append(" WHERE ");
//             criterias.GetType().GetProperties();
//             var propertyInfos = criterias.GetType().GetProperties().ToArray();
//             for (var i = 0; i < propertyInfos.Length; i++) {
//                 var propertyToUse = propertyInfos.ElementAt(i);

//                 object v = propertyInfos.ElementAt(i).GetValue(criterias, null);
//                 if (v != null) {
//                     //string n = propertyInfos.ElementAt(i).GetType().Name;
//                     whereClause.AppendFormat("[{0}] LIKE %{1}%", GetColumnName(propertyToUse), v);
//                     if (i < propertyInfos.Count() - 1)
//                         whereClause.AppendFormat(" AND ");
//                 }
//             }

//             return whereClause.ToString();
//         }

//         private string GetTableName(Type type) {
//             //var tableName = String.Format("[{0}]", type.Name);
//             var tableName = Encapsulate(type.Name);

//             var tableattr = type.GetCustomAttributes(true).SingleOrDefault(attr => attr.GetType().Name == "TableAttribute") as dynamic;
//             if (tableattr != null) {
//                 //tableName = String.Format("[{0}]", tableattr.Name);
//                 tableName = Encapsulate(tableattr.Name);
//                 try {
//                     if (!String.IsNullOrEmpty(tableattr.Schema)) {
//                         //tableName = String.Format("[{0}].[{1}]", tableattr.Schema, tableattr.Name);
//                         string schemaName = Encapsulate(tableattr.Schema);
//                         tableName = String.Format("{0}.{1}", schemaName, tableName);
//                     }
//                 } catch (RuntimeBinderException) {
//                     //Schema doesn't exist on this attribute.
//                 }
//             }

//             return tableName;
//         }

//         private static string GetColumnName(PropertyInfo propertyInfo) {
//             var columnName = Encapsulate(propertyInfo.Name);

//             var columnattr = propertyInfo.GetCustomAttributes(true).SingleOrDefault(attr => attr.GetType().Name == "ColumnAttribute") as dynamic;
//             if (columnattr != null) {
//                 columnName = Encapsulate(columnattr.Name);
//             }
//             return columnName;
//         }

//         private static string Encapsulate(string databaseword) {
//             return string.Format(_encapsulation, databaseword);
//         }

//         #endregion

//         #region Master management methods

//         public async Task<ResponseModel<ProgramInfoModel>> GetProgramInfo(ProgramInfoModel programInfoModel) {
//             ResponseModel<ProgramInfoModel> returnResponse = new ResponseModel<ProgramInfoModel>();
//             ProgramInfoModel result = null;

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     var query = "GetProgramInfo";

//                     var dynamicParameters = new DynamicParameters();
//                     dynamicParameters.Add("@ORGANIZE_ID", programInfoModel.ORGANIZE_ID);
//                     dynamicParameters.Add("@ROLE_CODE", programInfoModel.ROLE_CODE);
//                     dynamicParameters.Add("@PROGRAM_CODE", programInfoModel.PROGRAM_CODE);

//                     dynamic entity = await SqlMapper.QuerySingleOrDefaultAsync<dynamic>(connection, query, dynamicParameters, commandType: System.Data.CommandType.StoredProcedure);
//                     if (entity != null) {
//                         result = new ProgramInfoModel() { ORGANIZE_ID = entity.ORGANIZE_ID, ROLE_CODE = entity.ROLE_CODE, PROGRAM_CODE = entity.PROGRAM_CODE, PROGRAM_NAME = entity.PROGRAM_NAME, PROGRAM_LINK = entity.PROGRAM_LINK };
//                     } else {
//                         throw new Exception("Invalid authorization! [Organize, Role and Program]");
//                     }

//                     returnResponse.Entity = result;
//                     returnResponse.ReturnStatus = true;
//                 }
//             } catch (Exception ex) {
//                 returnResponse.ReturnStatus = false;
//                 returnResponse.ReturnMessage.Add(ex.Message);
//             }

//             return returnResponse;
//         }

//         public async Task<ServiceResults<T>> GetEntities(int currentPageIndex, string sortExpression, string sortDirection) {
//             ServiceResults<T> results = new ServiceResults<T>();
//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     IEnumerable<T> entities = await connection.GetList<T>();
//                     entities = QueryableExtensions.OrderByDynamic<T>(entities.AsQueryable(), sortExpression, sortDirection.Equals("asc") ? QueryableExtensions.Order.Asc : QueryableExtensions.Order.Desc);
//                     IList<T> pagingEntities = GetPagination(entities.ToList(), currentPageIndex);
//                     int cnt = pagingEntities != null ? entities.Count() : 0;

//                     results.Entities = pagingEntities;
//                     results.PageSize = pageSize;
//                     results.TotalRows = cnt;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> GetEntitiesByWhereClause(string whereClause, int currentPageIndex, string sortExpression, string sortDirection) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     IEnumerable<T> entities = null;

//                     if (whereClause == null) {
//                         entities = await connection.GetList<T>();
//                     } else {
//                         entities = await connection.GetList<T>(whereClause);
//                     }

//                     if (!string.IsNullOrEmpty(sortExpression))
//                         entities = QueryableExtensions.OrderByDynamic<T>(entities.AsQueryable(), sortExpression, sortDirection.Equals("asc") ? QueryableExtensions.Order.Asc : QueryableExtensions.Order.Desc);

//                     IList<T> pagingEntities = GetPagination(entities.ToList(), currentPageIndex);
//                     int cnt = pagingEntities != null ? entities.Count() : 0;

//                     results.Entities = pagingEntities;
//                     results.PageSize = pageSize;
//                     results.TotalRows = cnt;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> GetEntitiesByCriteria(object whereConditions, int currentPageIndex, string sortExpression, string sortDirection) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     IEnumerable<T> entities = null;

//                     entities = await connection.GetList<T>(whereConditions);
//                     entities = QueryableExtensions.OrderByDynamic<T>(entities.AsQueryable(), sortExpression, sortDirection.Equals("asc") ? QueryableExtensions.Order.Asc : QueryableExtensions.Order.Desc);

//                     IList<T> pagingEntities = GetPagination(entities.ToList(), currentPageIndex);
//                     int cnt = pagingEntities != null ? entities.Count() : 0;

//                     results.Entities = pagingEntities;
//                     results.PageSize = pageSize;
//                     results.TotalRows = cnt;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> GetEntitiesByCriteria(object whereConditions, string sortExpression, string sortDirection) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     IEnumerable<T> entities = null;

//                     if (whereConditions == null)
//                         entities = await connection.GetList<T>();
//                     else
//                         entities = await connection.GetList<T>(whereConditions);

//                     sortDirection = string.IsNullOrEmpty(sortDirection) ? "asc" : sortDirection;
//                     var currenttype = typeof(T);
//                     var idProps = GetIdProperties(currenttype).ToList();
//                     var onlyKey = idProps.First();
//                     sortExpression = string.IsNullOrEmpty(sortExpression) ? onlyKey.Name : sortExpression;

//                     entities = QueryableExtensions.OrderByDynamic<T>(entities.AsQueryable(), sortExpression, sortDirection.Equals("asc") ? QueryableExtensions.Order.Asc : QueryableExtensions.Order.Desc);

//                     results.Entities = entities;
//                     results.PageSize = pageSize;
//                     results.TotalRows = entities.ToList().Count;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> SelectEntity(int id) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     var entity = await connection.Get<T>(id);
//                     results.Entity = entity;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> SelectEntity(T entity) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     var result = await connection.Get<T>(entity);
//                     results.Entity = result;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> InsertEntity(T entity) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     int result = await connection.Insert<int>(entity);

//                     var newEntity = await connection.Get<T>(result);
//                     results.Entity = newEntity;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task<ServiceResults<T>> UpdateEntity(T entity) {
//             ServiceResults<T> results = new ServiceResults<T>();

//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     var result = await connection.Update(entity);

//                     results.Entity = entity;
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }

//             return results;
//         }

//         public async Task DeleteEntity(int id) {
//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     await connection.Delete<T>(id);
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }
//         }

//         public async Task DeleteEntity(T entity) {
//             try {
//                 using (SqlConnection connection = GetConnection()) {
//                     await connection.Delete<T>(entity);
//                 }
//             } catch (Exception ex) {
//                 throw ex;
//             }
//         }

//         #endregion

//         #region Helpers

//         protected IList<T> GetPagination(IList<T> list, int currentPageIndex) {
//             return list.Skip(currentPageIndex * pageSize).Take(pageSize).ToList();
//         }

//         private IEnumerable<PropertyInfo> GetIdProperties(Type type) {
//             var tp = type.GetProperties().Where(p => p.GetCustomAttributes(true).Any(attr => attr.GetType().Name == "KeyAttribute")).ToList();
//             return tp.Any() ? tp : type.GetProperties().Where(p => p.Name == "Id");
//         }

//         #endregion

//         #region Validation

//         public virtual bool ValidateCriteria() {
//             return true;
//         }


//         public virtual bool ValidateEntity() {
//             return true;
//         }

//         #endregion
//     }

//     public static class QueryableExtensions {
//         public enum Order {
//             Asc,
//             Desc
//         }

//         public static IQueryable<T> OrderByDynamic<T>(
//             this IQueryable<T> query,
//             string orderByMember,
//             Order direction) {
//             var queryElementTypeParam = Expression.Parameter(typeof(T));

//             var memberAccess = Expression.PropertyOrField(queryElementTypeParam, orderByMember);

//             var keySelector = Expression.Lambda(memberAccess, queryElementTypeParam);

//             var orderBy = Expression.Call(
//                 typeof(Queryable),
//                 direction == Order.Asc ? "OrderBy" : "OrderByDescending",
//                 new Type[] { typeof(T), memberAccess.Type },
//                 query.Expression,
//                 Expression.Quote(keySelector));

//             return query.Provider.CreateQuery<T>(orderBy);
//         }
//     }
// }
