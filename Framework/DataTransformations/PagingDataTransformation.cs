﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.DataTransformations
{
    public class PagingDataTransformation
    {
        public int CurrentPageIndex { get; set; }
        public int PageSize { get; set; }
        public string SortDirection { get; set; }
        public string SortExpression { get; set; }
    }
}
