﻿using Framework.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Controllers
{
    public class ReportControllerBase<T> : ControllerBase
    {

        protected readonly ILogger _logger;
        protected readonly ApplicationInfo _appInfo;

        public ReportControllerBase(IOptions<ApplicationInfo> appInfo, ILogger<T> logger) {

            _logger = logger;
            _appInfo = appInfo.Value;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { string.Format("Version: 1.0.0, IsDevelopment:Yes") };
        }
    }
}
