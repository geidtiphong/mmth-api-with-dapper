﻿using Framework.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Controllers {
    public class ProcessControllerBase<T> : ControllerBase {

        protected readonly ILogger _logger;
        protected readonly ApplicationInfo _appInfo;

        public ProcessControllerBase(IOptions<ApplicationInfo> appInfo, ILogger<T> logger) {

            _logger = logger;
            _appInfo = appInfo.Value;
        }

        [HttpGet]
        public ActionResult<ApplicationInfo> Get() {
            return _appInfo;
        }
    }
}
