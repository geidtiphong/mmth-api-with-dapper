﻿using Framework.Services;
using Framework.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Framework.Controllers
{
    public class AppControllerBase<T> : ControllerBase
    {
        public IConfiguration configuration { get; }
        protected readonly ILogger _logger;
        protected readonly ApplicationInfo _appInfo;

        public AppControllerBase(IOptions<ApplicationInfo> appInfo, ILogger<T> logger)
        {
            _logger = logger;
            _appInfo = appInfo.Value;
        }

        [HttpGet]
        [Route("Version")]
        public ActionResult<ApplicationInfo> Version()
        {
            return _appInfo;
        }
    }
}
