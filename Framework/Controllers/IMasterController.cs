﻿using Framework.DataTransformations;
using Framework.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Framework.Controllers
{
    public interface IMasterController<T,K>
    {
        //Task<IActionResult> ProgramInfo([FromBody] ProgramInfoModel programInfo);
        Task<IActionResult> Search([FromBody] K criteriaDataTransformation);
        Task<IActionResult> Select([FromBody] T dataTransformation);
        Task<IActionResult> Insert([FromBody] T dataTransformation);
        Task<IActionResult> Update([FromBody] T dataTransformation);
        Task<IActionResult> Delete([FromBody] T dataTransformation);
    }
}
