﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace Framework.Controllers
{
    public abstract class MasterControllerBase : ControllerBase
    {       
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { string.Format("Version: 1.0.0, IsDevelopment:Yes") };
        }
    }
}
