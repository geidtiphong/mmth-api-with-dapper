﻿namespace Framework.Utilities
{
    public class ApplicationInfo
    {
        public string FrameworkVersion { get; set; }
        public string ApiVersion { get; set; }
    }
}
