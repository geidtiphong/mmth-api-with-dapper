﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
using Framework.Models;

namespace Framework.Utilities
{
    public static class ConfigurationUtility
    {
		public static ConnectionStrings GetConnectionStrings()
		{
            /*
			string environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

			string jsonFile = $"appsettings.{environment}.json";

			var builder = new ConfigurationBuilder()
			.SetBasePath(Directory.GetCurrentDirectory())
			.AddJsonFile(jsonFile, optional: false, reloadOnChange: true)
			.AddEnvironmentVariables();

			IConfigurationRoot configuration = builder.Build();

			ConnectionStrings connectionStrings = new ConnectionStrings();
			configuration.GetSection("ConnectionStrings").Bind(connectionStrings);
            */
            var connectionStrings = new ConnectionStrings { PrimaryDatabaseConnectionString = "Data Source=vaio;Database=MDFN;User ID=sa;Password=admin2014;Trusted_Connection=True" };

            return connectionStrings;
		}
    }
}
