﻿using System;
using System.Data.SqlClient;
using System.Net;
using System.Threading.Tasks;
using Framework.Models;
using Framework.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace WebApi.GlobalErrorHandling.Extensions {
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ExceptionMiddleware {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ExceptionMiddleware(RequestDelegate next, ILogger logger) {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext) {
            try {
                await _next(httpContext);
            } catch (Exception ex) {
                _logger.LogError($"Something went wrong: {ex}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception) {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            //context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            ResponseModel<string> responseModel = new ResponseModel<string>();
            responseModel.ReturnStatus = false;

            if (exception.GetType() == typeof(SqlException)) {
                SqlException ex = (SqlException)exception;

                switch (ex.Number) {
                    case 547:
                        responseModel.ReturnMessage.Add("FOREIGN KEY");
                        break;
                    case 2627:
                        responseModel.ReturnMessage.Add("Unique key");
                        break;
                    case 2601:
                        //if(!string.IsNullOrEmpty(ex.Message)) 
                        responseModel.ReturnMessage.Add("Data dupplicate");
                        break;
                    default:
                        responseModel.ReturnMessage.Add(exception.Message);
                        break;
                }
            } else {
                responseModel.ReturnMessage.Add(exception.Message);
            }

            return context.Response.WriteAsync(SerializationFunction<ResponseModel<string>>.ReturnStringFromObject(responseModel));
        }

    }

}
