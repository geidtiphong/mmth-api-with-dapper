﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Framework.Models;
using Framework.Utilities;
using Hangfire;
using Hangfire.SqlServer;
using WebApi.GlobalErrorHandling.Extensions;
using WebApi.Services.Common;
using WebApi.Services.Common.Applications;
using WebApi.Services.Common.Login;
using WebApi.Services.Common.Select;
using WebApi.Services.Common.Excel;
using WebApi.Services.Common.Files;
using WebApi.Services.Master.IM2001M;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;
using System.IO;
using Microsoft.Extensions.FileProviders;
using System.Globalization;
using System.Threading;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Hosting;
using System.Text.Json;

namespace WebApi
{
    public class Startup
    {

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver());

            CorsPolicyBuilder corsBuilder = new CorsPolicyBuilder();

            corsBuilder.AllowAnyHeader();
            corsBuilder.AllowAnyMethod();
            corsBuilder.AllowAnyOrigin();
            // corsBuilder.AllowCredentials();
            corsBuilder.WithExposedHeaders("Content-Disposition");

            services.AddCors(options =>
            {
                options.AddPolicy("SiteCorsPolicy", corsBuilder.Build());
            });

            ConnectionStrings connectionStrings = new ConnectionStrings();
            Configuration.GetSection("ConnectionStrings").Bind(connectionStrings);

            ApplicationInfo applicationInfo = new ApplicationInfo();
            Configuration.GetSection("ApplicationInfo").Bind(applicationInfo);

            services.Configure<ConnectionStrings>(settings =>
            {
                settings.PrimaryDatabaseConnectionString = connectionStrings.PrimaryDatabaseConnectionString;
                // settings.Press1_DatabaseConnectionString = connectionStrings.Press1_DatabaseConnectionString;
                // settings.Press2_DatabaseConnectionString = connectionStrings.Press2_DatabaseConnectionString;
                // settings.Blanking_DatabaseConnectionString = connectionStrings.Blanking_DatabaseConnectionString;
            });

            services.Configure<ApplicationInfo>(settings =>
            {
                settings.FrameworkVersion = applicationInfo.FrameworkVersion;
                settings.ApiVersion = applicationInfo.ApiVersion;
            });

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
            });



            #region Services Common

            services.AddTransient<InfoService>();
            services.AddTransient<AuthorizationBusinessService>();
            services.AddTransient<ApplicationBusinessService>();
            services.AddTransient<ExcelFileGeneratorStreamController>();

            #endregion

            #region Services Master

            services.AddTransient<IM2001MBusinessService>();

            #endregion

            #region Services Process


            #endregion

            #region Services Interface


            #endregion

            #region Services Report

            #endregion

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = "https://mitsubishi-motors.com",
                    ValidAudience = "https://mitsubishi-motors.com",
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("Framework.Utilities.TokenManagement"))
                };
            });

            #region Hangfire

            // Add Hangfire services.
            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(Configuration.GetConnectionString("PrimaryDatabaseConnectionString"), new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                }));

            // Add the processing server as IHostedService
            services.AddHangfireServer();

            #endregion

            // Gzip
            services.AddResponseCompression();

            services.AddScoped<SecurityFilter>();
            services.Configure<ApplicationInfo>(Configuration.GetSection("ApplicationInfo"));

            // AddHttpContextAccessor
            services.AddHttpContextAccessor();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            // Custom response invalid model
            services.Configure<ApiBehaviorOptions>(options => options.InvalidModelStateResponseFactory = context =>
            {

                var errorMessage = context.ModelState.Values.SelectMany(x => x.Errors)
            .Select(x => x.ErrorMessage);

                ResponseModel<string> responseModel = new ResponseModel<string>();
                responseModel.ReturnStatus = false;
                responseModel.ReturnMessage.AddRange(errorMessage.ToArray());

                return new BadRequestObjectResult(responseModel);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IBackgroundJobClient backgroundJobs, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            // app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors("SiteCorsPolicy");
            app.UseResponseCompression();
            app.UseHangfireDashboard();

            backgroundJobs.Enqueue(() => Console.WriteLine("Hello world from Hangfire!"));

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // Specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            // app.UseMiddleware<ExceptionMiddleware>();

            // Gzip
            app.UseResponseCompression();

            // Access Directory
            Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Excels"));
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                Path.Combine(Directory.GetCurrentDirectory(), "Excels")),
                RequestPath = "/Excels",
                EnableDirectoryBrowsing = true
            });


            app.UseRouting();
            // app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
