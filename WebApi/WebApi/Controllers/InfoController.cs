﻿using System.Collections.Generic;
using Framework.Models;
using Framework.Utilities;
using WebApi.Services.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        private readonly InfoService _service;
        private readonly ILogger _logger;
        private readonly IOptions<ApplicationInfo> _appInfo;
        private readonly IOptions<ConnectionStrings> _connectionInfo;

        public InfoController(InfoService service,IOptions<ConnectionStrings> connectionInfo, IOptions<ApplicationInfo> appInfo, ILogger<InfoController> logger)
        {
            _service = service;
            _appInfo = appInfo;
            _connectionInfo = connectionInfo;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            var databaseInfo = string.Format("Database Info: {0}", _service.GetDatabaseInfo());            
            var frameworkVersion = string.Format("Framework Version: {0}", _appInfo.Value.FrameworkVersion);
            var apiVersion = string.Format("API Version: {0}", _appInfo.Value.ApiVersion);

            return new string[] { frameworkVersion, apiVersion, databaseInfo };
        }
    }
}
