using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using Framework.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Services.Entities;

namespace WebApi.Controllers {
  [Route ("api/[controller]")]
  [ApiController]
  public class BlogController : ControllerBase {
    protected SqlConnection connection;
    protected string _connectionString;
    public BlogController (IOptions<ConnectionStrings> config) {
      _connectionString = config.Value.PrimaryDatabaseConnectionString;
      connection = new SqlConnection (_connectionString);
    }

    [HttpGet]
    [Route ("Search")]
    public async Task<IActionResult> Search () {
      ResponseModel<IEnumerable<Blog>> returnResponse = new ResponseModel<IEnumerable<Blog>> ();

      returnResponse.ReturnStatus = true;
      returnResponse.Entity = await connection.GetAllAsync<Blog> ();
      return Ok (returnResponse);
    }

  }
}