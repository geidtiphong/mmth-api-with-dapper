﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Framework.Models;
using WebApi.Services.Master.IM2001M;
using Microsoft.AspNetCore.Mvc;
using WebApi.Services.Entities;

namespace WebApi.Controllers.Master
{
    [Route("api/[controller]")]
    [ApiController]
    public class IM2001MController : ControllerBase
    {
        private readonly IM2001MBusinessService _businessService;

        public IM2001MController(IM2001MBusinessService service)
        {
            _businessService = service;
        }

        [HttpGet]
        // [HttpPost]
        [Route("Search")]
        public async Task<IActionResult> Search()
        {
            ResponseModel<IEnumerable<TBL_USER>> returnResponse = new ResponseModel<IEnumerable<TBL_USER>>();

            returnResponse.ReturnStatus = true;
            returnResponse.Entity = await _businessService.Search();
            return Ok(returnResponse);
        }

        [HttpPost]
        [Route("SearchById")]
        public async Task<IActionResult> SearchById(TBL_USER data)
        {
            ResponseModel<TBL_USER> returnResponse = new ResponseModel<TBL_USER>();

            returnResponse.ReturnStatus = true;
            returnResponse.Entity = await _businessService.SearchById(data);
            return Ok(returnResponse);
        }

        [HttpGet]
        // [HttpPost]
        [Route("SearchByStore")]
        public async Task<IActionResult> SearchByStore()
        {
            ResponseModel<IEnumerable<TBL_USER>> returnResponse = new ResponseModel<IEnumerable<TBL_USER>>();

            returnResponse.ReturnStatus = true;
            returnResponse.Entity = await _businessService.SearchByStore();
            return Ok(returnResponse);
        }

        [HttpPost]
        [Route("SearchWithStoreById")]
        public async Task<IActionResult> SearchWithStoreById(TBL_USER data)
        {
            ResponseModel<TBL_USER> returnResponse = new ResponseModel<TBL_USER>();

            returnResponse.ReturnStatus = true;
            returnResponse.Entity = await _businessService.SearchWithStoreById(data);
            return Ok(returnResponse);
        }


        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create(TBL_USER data)
        {
            ResponseModel<TBL_USER> returnResponse = new ResponseModel<TBL_USER>();
            returnResponse.Entity = await _businessService.Create(data);
            returnResponse.ReturnStatus = true;
            return Created("", returnResponse);
        }

        [HttpPut]
        // [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update(TBL_USER data)
        {
            ResponseModel<TBL_USER> returnResponse = new ResponseModel<TBL_USER>();
            returnResponse.Entity = await _businessService.Update(data);
            returnResponse.ReturnStatus = true;
            return Ok(returnResponse);
        }

        [HttpDelete]
        // [HttpPost]
        [Route("Delete")]
        public async Task<IActionResult> Delete(TBL_USER data)
        {
            ResponseModel<TBL_USER> returnResponse = new ResponseModel<TBL_USER>();
            await _businessService.Delete(data);
            returnResponse.Entity = null;
            returnResponse.ReturnStatus = true;
            return NoContent();
        }


    }
}