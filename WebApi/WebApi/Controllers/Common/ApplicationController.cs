﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Framework.Models;
using WebApi.Services.Common.Applications;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers.Common {
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationController : ControllerBase {
        private readonly ApplicationBusinessService _businessService;
        private readonly ILogger _logger;

        public ApplicationController(
            ApplicationBusinessService service,
            ILogger<ApplicationController> logger) {

            _logger = logger;
            _businessService = service;
        }

        [HttpPost]
        [Route("ProgramInfo")]
        public async Task<IActionResult> ProgramInfo([FromBody] ProgramInfoModel programInfo) {
            ResponseModel<ProgramInfoModel> returnResponse = new ResponseModel<ProgramInfoModel>();

            try {
                returnResponse = await _businessService.GetProgramInfo(programInfo);

                returnResponse.ReturnStatus = true;

                return Ok(returnResponse);
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
                return BadRequest(returnResponse);
            }
        }
    }
}