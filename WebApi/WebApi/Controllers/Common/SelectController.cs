﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Framework.Controllers;
using Framework.Models;
using WebApi.Services.Common.Login;
using WebApi.Services.Common.Select;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers.Common
{
    [Route("api/[controller]")]
    [ApiController]
    public class SelectController : ControllerBase
    {
        private readonly AuthorizationBusinessService _businessService;
        public IConfiguration configuration { get; }
        private readonly ILogger _logger;

        /// <summary>
		/// Authorization Controller
		/// </summary>
		public SelectController(AuthorizationBusinessService businessService, ILogger<AuthorizationController> logger)
        {
            _businessService = businessService;
            _logger = logger;
        }

        [HttpPost]        
        [Route("GetOrganizesByRole")]
        public async Task<IActionResult> GetOrganizesByRole([FromBody] RoleDataTransformation parameter)
        {
            ResponseModel<IEnumerable<SelectDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectDataTransformation>>();
            //SelectDataTransformation dataTransformation = null;

            try
            {
                returnResponse = await _businessService.GetOrganizesByRole(parameter.RoleCode);                
                returnResponse.ReturnStatus = true;

                return Ok(returnResponse);
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
                return BadRequest(returnResponse);
            }
        }
    }
}
