﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Framework.Controllers;
using Framework.Helpers;
using Framework.Models;
using Framework.Utilities;
using WebApi.Services.Common.Login;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace WebApi.Controllers.Common
{

    [Route("api/[controller]")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        private readonly AuthorizationBusinessService _businessService;
        public IConfiguration configuration { get; }
        private readonly ILogger _logger;

        /// <summary>
		/// Authorization Controller
		/// </summary>
		public AuthorizationController(AuthorizationBusinessService businessService, ILogger<AuthorizationController> logger)
        {
            _businessService = businessService;
            _logger = logger;
        }

        [HttpPost]
        [Route("GetMenusByOrganizeRole")]
        public async Task<IActionResult> GetMenusByOrganizeRole([FromBody] MenuRequestDataTransformation parameter)
        {
            ResponseModel<TreeNode> returnResponse = new ResponseModel<TreeNode>();

            try {
                returnResponse.Entity = await _businessService.GetMenusByOrganizeRole(parameter);
                returnResponse.ReturnStatus = true;

                return Ok(returnResponse);
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
                return BadRequest(returnResponse);
            }
        }
       

        [HttpGet]
        [Route("GetRoles")]
        public async Task<IActionResult> GetRoles()
        {
            ResponseModel<IEnumerable<RoleDataTransformation>> returnResponse = new ResponseModel<IEnumerable<RoleDataTransformation>>();

            try
            {
                returnResponse = await _businessService.GetRoles();
                returnResponse.ReturnStatus = true;

                return Ok(returnResponse);
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
                return BadRequest(returnResponse);
            }
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login([FromBody] LoginDataTransformation dataTransformation)
        {
            ResponseModel<LoginDataTransformation> returnResponse = new ResponseModel<LoginDataTransformation>();
            try
            {
                returnResponse = await _businessService.Login(dataTransformation);

                if (returnResponse.ReturnStatus == true)
                {
                    int userId = returnResponse.Entity.UserId;
                    //int accountId = returnResponse.Entity.AccountId;
                    string firstName = returnResponse.Entity.FirstName;
                    string lastName = returnResponse.Entity.LastName;
                    //string userName = returnResponse.Entity.UserName;
                    //string companyName = returnResponse.Entity.CompanyName;

                    string tokenString = TokenManagement.CreateToken(userId, firstName, lastName);
                    returnResponse.Entity.IsAuthenicated = true;
                    returnResponse.Entity.Token = tokenString;
                    return Ok(returnResponse);
                }
                else
                {
                    return BadRequest(returnResponse);
                }

            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
                return BadRequest(returnResponse);
            }

        }

        [HttpPost]
        [Route("Logout")]
        public async Task<IActionResult> Logout([FromBody] LoginDataTransformation dataTransformation)
        {
            ResponseModel<LoginDataTransformation> returnResponse = new ResponseModel<LoginDataTransformation>();
            try
            {
                returnResponse = await _businessService.Login(dataTransformation);

                if (returnResponse.ReturnStatus == true)
                {
                    int userId = returnResponse.Entity.UserId;
                    //int accountId = returnResponse.Entity.AccountId;
                    string firstName = returnResponse.Entity.FirstName;
                    string lastName = returnResponse.Entity.LastName;
                    //string emailAddress = returnResponse.Entity.UserName;
                    //string companyName = returnResponse.Entity.CompanyName;

                    string tokenString = TokenManagement.CreateToken(userId, firstName, lastName);
                    returnResponse.Entity.IsAuthenicated = true;
                    returnResponse.Entity.Token = tokenString;
                    return Ok(returnResponse);
                }
                else
                {
                    return BadRequest(returnResponse);
                }

            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
                return BadRequest(returnResponse);
            }

        }
    }    
}