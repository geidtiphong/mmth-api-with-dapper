﻿using Dapper;
using Framework.Models;
using Framework.Services;
using Framework.Utilities;
using WebApi.Services.Common.Excel;
using WebApi.Services.Common.Select;
using WebApi.Services.Entities;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace WebApi.Services.Master.IM2001M
{
    public class IM2001MBusinessService
    {
        protected IDbConnection connection;
        protected string _connectionString;
        public IM2001MBusinessService(IOptions<ConnectionStrings> config)
        {
            _connectionString = config.Value.PrimaryDatabaseConnectionString;
            connection = new SqlConnection(_connectionString);

        }
        public async Task<IEnumerable<TBL_USER>> Search(TBL_USER data = null)
        {
            var results = await connection.GetAllAsync<TBL_USER>();
            return results;
        }

         public async Task<TBL_USER> SearchById(TBL_USER data = null)
        {
            var results = await connection.GetAsync<TBL_USER>(data.id);
            return results;
        }

        public async Task<IEnumerable<TBL_USER>> SearchByStore(TBL_USER data = null)
        {
            var results = await connection.QueryAsync<TBL_USER>("MY_USERS", null, commandType: CommandType.StoredProcedure);
            return results;
        }

        public async Task<TBL_USER> SearchWithStoreById(TBL_USER data = null)
        {
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@id", data.id);
            var results = await connection.QueryFirstOrDefaultAsync<TBL_USER>("MY_USERS_BY_ID", parameters, commandType: CommandType.StoredProcedure);
            return results;
        }

        public async Task<TBL_USER> Create(TBL_USER data)
        {
            var id = await connection.InsertAsync(data);
            data.id = id;
            return data;
        }

        public async Task<TBL_USER> Update(TBL_USER data)
        {
            var results = await connection.UpdateAsync(data);
            return data;
        }

        public async Task<bool> Delete(TBL_USER data)
        {
            return await connection.DeleteAsync<TBL_USER>(data);
        }

    }
}
