﻿using Framework.DataTransformations;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Master.IM2001M
{
    public class IM2001M01CriteriaDataTransformation : PagingDataTransformation
    {
        public string FILE_PATH { get; set; }
        public string UPLOAD_FILE_TYPE { get; set; }

        public string ACTIVE_TYPE { get; set; }
        public string ITEM_NO { get; set; }
        public string OPN_NO { get; set; }
        public int ORGANIZE_ID { get; set; }

        public string UPDATED_BY { get; set; }
    }

}
