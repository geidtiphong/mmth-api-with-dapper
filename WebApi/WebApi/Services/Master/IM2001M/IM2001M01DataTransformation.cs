﻿using Framework.DataTransformations;
using WebApi.Services.Common.Excel;
using WebApi.Services.Common.Files;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace WebApi.Services.Master.IM2001M
{
    public class IM2001M01DataTransformation : IDataTransformation
    {
        public string ORGANIZE_NAME { get; set; }
        public int ROUTING_ID { get; set; }

        [StringLength(40)]
        public string ITEM_NO { get; set; }
        public string ITEM_DES { get; set; }
        public string WC_NO { get; set; }
        public string WC_DES { get; set; }
        public int OPN_NO { get; set; }

        public decimal PCS_PER_HR { get; set; }
        public decimal HR_PER_PCS { get; set; }

        public DateTime START_EFF_DATE { get; set; }
        public DateTime END_EFF_DATE { get; set; }

        [StringLength(1)]
        public string CONTROL_POINT { get; set; }
        
        public string BACKFLUSH_FLAG { get; set; }

        [StringLength(1)]
        public string OUTSIDE_FLAG { get; set; }
        public decimal SPH { get; set; }
        public int PPS { get; set; }
        public string TURN_OVER { get; set; }

        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
        public int ORGANIZE_ID { get; set; }

    }

    public class IM2001M01DataResult
    {
        public int RESULT { get; set; }
    }

    public class IM2001M01FileAndExcelDataTransformation : FileDataTransformation
    {
        public List<IM2001M01ExcelFullDataTransformation> EXCEL_DATA { get; set; }
    }

    public static class IM2001MTemplateDisplay
    {
        public static IEnumerable<ExcelCellSetting> columnSetting = new List<ExcelCellSetting>()
        {
            new ExcelCellSetting() { Source = "ITEM_NO",             Header = "Item No" },
            new ExcelCellSetting() { Source = "OPN_NO",              Header = "Operation Number" },
            new ExcelCellSetting() { Source = "WC_NO",               Header = "Work Center Number" },
            new ExcelCellSetting() { Source = "PCS_PER_HR",          Header = "Pieces / Hours",                           CellFormat = "#,##0.00000" },
            new ExcelCellSetting() { Source = "START_EFF_DATE",      Header = "Start Effective Date (YYYY-MM-DD)",        CellFormat = "yyyy-MM-dd", },
            new ExcelCellSetting() { Source = "END_EFF_DATE",        Header = "End Effective Date (YYYY-MM-DD)",          CellFormat = "yyyy-MM-dd", },
            new ExcelCellSetting() { Source = "CONTROL_POINT_NAME",  Header = "Control Point (Yes/No)" },
            new ExcelCellSetting() { Source = "BACKFLUSH_FLAG_NAME", Header = "Backflush Flag" },
            new ExcelCellSetting() { Source = "OUTSIDE_FLAG_NAME",   Header = "Outside Flag (Yes/No)"},
            new ExcelCellSetting() { Source = "SPH",                 Header = "Stroke Per Hours",            CellFormat="#,##0.00000" },
            new ExcelCellSetting() { Source = "PPS",                 Header = "Pieces Per Stroke",},
            new ExcelCellSetting() { Source = "TURN_OVER_NAME",      Header = "Turn Over" },
        };
    }

    //================= For return all Excel data reading ==============================
    public class IM2001M01ExcelFullDataTransformation
    {
        public string RESULT { get; set; }

        public string HR_PER_PCS { get; set; }
        public string ITEM_NO { get; set; }
        public string OPN_NO { get; set; }
        public string WC_NO { get; set; }
        public string PCS_PER_HR { get; set; }
        public string START_EFF_DATE { get; set; }
        public string END_EFF_DATE { get; set; }
        public string CONTROL_POINT_NAME { get; set; }
        public string CONTROL_POINT { get; set; }
        public string BACKFLUSH_FLAG_NAME { get; set; }
        public string BACKFLUSH_FLAG { get; set; }
        public string OUTSIDE_FLAG_NAME { get; set; }
        public string OUTSIDE_FLAG { get; set; }
        public string SPH { get; set; }
        public string PPS { get; set; }
        public string TURN_OVER_NAME { get; set; }
        public string TURN_OVER { get; set; }
    }

    //================= For validate Excel data ==============================
    public class IM2001M01ExcelDataValidator 
    {
        public string RESULT { get; set; }

        public decimal? HR_PER_PCS { get; set; }

        [Required]
        [StringLength(30)]
        public string ITEM_NO { get; set; }
        [Required]
        public int? OPN_NO { get; set; }
        [Required]
        public string WC_NO { get; set; }
        [Required]
        public decimal? PCS_PER_HR { get; set; }
        [Required]
        public DateTime? START_EFF_DATE { get; set; }
        [Required]
        public DateTime? END_EFF_DATE { get; set; }
        [Required]
        public string CONTROL_POINT_NAME { get; set; }
        public string CONTROL_POINT { get; set; }
        [Required]
        public string BACKFLUSH_FLAG_NAME { get; set; }
        public string BACKFLUSH_FLAG { get; set; }
        [Required]
        public string OUTSIDE_FLAG_NAME { get; set; }
        public string OUTSIDE_FLAG { get; set; }
        public decimal? SPH { get; set; }
        public int? PPS { get; set; }
        public string TURN_OVER_NAME { get; set; }
        public string TURN_OVER { get; set; }
    }

    public static class IM2001MDisplay 
    {
        public static IEnumerable<ExcelCellSetting> columnSetting = new List<ExcelCellSetting>()
        {
            new ExcelCellSetting(){ Source = "ORGANIZE_NAME",       Header = "Organize Name" },

            new ExcelCellSetting(){ Source = "ITEM_NO",             Header = "Item No" },
            new ExcelCellSetting(){ Source = "OPN_NO",              Header = "Operation Number" },
            new ExcelCellSetting(){ Source = "WC_NO",               Header = "Work Center Number" },
            new ExcelCellSetting(){ Source = "PCS_PER_HR",          Header = "Pieces / Hours",              CellFormat="#,##0.00000" },
            new ExcelCellSetting(){ Source = "HR_PER_PCS",          Header = "Hours / Pieces",              CellFormat="#,##0.00000" },
            new ExcelCellSetting(){ Source = "START_EFF_DATE",      Header = "Start Effective Date",        CellFormat="yyyy-MM-dd",               CustomHeader="(YYYY-MM-DD)" },
            new ExcelCellSetting(){ Source = "END_EFF_DATE",        Header = "End Effective Date",          CellFormat="yyyy-MM-dd",               CustomHeader="(YYYY-MM-DD)" },
            new ExcelCellSetting(){ Source = "CONTROL_POINT_NAME",  Header = "Control Point",               CustomHeader="(Yes/No)" },
            new ExcelCellSetting(){ Source = "BACKFLUSH_FLAG",      Header = "Backflush Flag" },
            new ExcelCellSetting(){ Source = "OUTSIDE_FLAG_NAME",   Header = "Outside Flag",                CustomHeader="(Yes/No)" },
            new ExcelCellSetting(){ Source = "SPH",                 Header = "Stroke Per Hours",            CellFormat="#,##0.00000" },
            new ExcelCellSetting(){ Source = "PPS",                 Header = "Pieces Per Stroke",},
            new ExcelCellSetting(){ Source = "TURN_OVER_NAME",      Header = "Turn Over" },

            new ExcelCellSetting(){ Source = "ROUTING_ID",          Header = "Routing ID" },
            new ExcelCellSetting(){ Source = "ITEM_DES",            Header = "Item Description" },  
            new ExcelCellSetting(){ Source = "WC_DES",              Header = "Work Center Description" },            
            new ExcelCellSetting(){ Source = "CREATED_DATE",        Header = "Created Date",                CellFormat="yyyy-MM-dd HH:mm:ss"  },
            new ExcelCellSetting(){ Source = "CREATED_BY",          Header = "Created By" },
            new ExcelCellSetting(){ Source = "UPDATED_DATE",        Header = "Updated Date",                CellFormat="yyyy-MM-dd HH:mm:ss"  },
            new ExcelCellSetting(){ Source = "UPDATED_BY",          Header = "Updated By" },
        };
    }
    }
