﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Master.IM2001M
{
    public class IM2001M01InquireDataTransformation : IM2001M01DataTransformation
    {
        public string CONTROL_POINT_NAME { get; set; }
        public string OUTSIDE_FLAG_NAME { get; set; }
        public string TURN_OVER_NAME { get; set; }

        public int TOTAL_RECORDS { get; set; }
        public int TOTAL_PAGES { get; set; }        
    }

    public class IM2001M01ProcessDataTransformation
    {
        public int RETURN_CODE { get; set; }
        public string RETURN_MESSAGE { get; set; }
    }
}
