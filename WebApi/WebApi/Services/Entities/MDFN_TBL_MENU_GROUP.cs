﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Services.Entities
{
    public class MDFN_TBL_MENU_GROUP
    {
        [Key]
        public int MDFN_TBL_MENU_GROUP_ID { get; set; }
        public int ORGANIZE_ID { get; set; }
        public string ORGANIZE_NAME { get; set; }
        public int PROGRAM_PARENT_ID { get; set; }
        public string PROGRAM_GROUP_NAME { get; set; }
        public int PROGRAM_GROUP_LEVEL { get; set; }
        public int PROGRAM_GROUP_SEQUENCE { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
    }
}
