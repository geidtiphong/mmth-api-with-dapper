using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Services.Entities {
    public class MDFN_TBL_USER {
        [Key]
        public int MDFN_TBL_USER_ID { get; set; }
        public string USER_NAME { get; set; }
        public string PASSWORD { get; set; }
        public string PASSWORDSALT { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
    }
}