﻿using System;
using Dapper.Contrib.Extensions;

namespace WebApi.Services.Entities
{
    [Table("TBL_USER")]
    public class TBL_USER
    {
        [Key]
        [Write(false)]
        public int id { get; set; } = 0;
        public string title { get; set; }
        public string body { get; set; }
    }
}