﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Services.Entities
{
    public class MDFN_TBL_MENU
    {
        [Key]
        public int MDFN_TBL_MENU_ID { get; set; }
        public int PROGRAM_GROUP_ID { get; set; }
        public string ROLE_CODE { get; set; }
        public string ROLE_DESCRIPTION { get; set; }
        public string PROGRAM_CODE { get; set; }
        public string PROGRAM_NAME { get; set; }
        public int PROGRAM_SEQUENCE { get; set; }
        public string PROGRAM_LINK { get; set; }
        public bool ADD_FLAG { get; set; }
        public bool EDIT_FLAG { get; set; }
        public bool REPORT_FLAG { get; set; }
        public bool DELETE_FLAG { get; set; }
        public bool PROCESS_FLAG { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? UPDATED_DATE { get; set; }
        public string UPDATED_BY { get; set; }
    }
}
