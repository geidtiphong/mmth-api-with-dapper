using Dapper.Contrib.Extensions;

namespace WebApi.Services.Entities {
  [Table ("Blog")]
  public class Blog {
    [Key]
    public int BlogId { get; set; } = 0;
    public string Url { get; set; }
  }
}