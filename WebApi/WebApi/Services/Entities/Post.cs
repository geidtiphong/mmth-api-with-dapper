using Dapper.Contrib.Extensions;

namespace WebApi.Services.Entities {
  [Table ("Post")]
  public class Post {

    [Key]
    public int PostId { get; set; } = 0;
    public int BlogId { get; set; } = 0;
    public string Content { get; set; }
    public string Title { get; set; }
  }
}