﻿using Framework.DataTransformations;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Files{
    public class FileDataTransformation 
    {
        public string FILE_PATH { get; set; }
        public string FILE_NAME { get; set; }
        public double FILE_SIZE { get; set; }
        public int ORGANIZE_ID { get; set; }
    }
}
