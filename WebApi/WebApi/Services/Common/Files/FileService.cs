﻿using Framework.Models;
using Microsoft.Extensions.Options;
using System;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using System.Data;
using Framework.Utilities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using Framework.Services;

namespace WebApi.Services.Common.Files
{
    public static class FileService
    {

        public static async Task<List<FileDataTransformation>> UploadFiles(List<IFormFile> files,string filePath = @".\TempFilesUploaded\")
        {

            List<FileDataTransformation> fileData = new List<FileDataTransformation>();

            if (files.Count == 0) throw new Exception("No any files for upload.");
            if (string.IsNullOrEmpty(filePath)) throw new Exception("String file path must be declare before upload.");

            //long size = files.Sum(f => f.Length);
            
            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);

            var filename = string.Empty;

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    filename = Guid.NewGuid().ToString() + Path.GetExtension(formFile.FileName);
                    using (var stream = new FileStream(filePath + filename, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                        fileData.Add(
                            new FileDataTransformation()
                            {
                                FILE_NAME = filename,
                                FILE_PATH = filePath + filename,
                                FILE_SIZE = formFile.Length
                            }
                        );
                    }
                }
            }

            // process uploaded files
            // Don't rely on or trust the FileName property without validation.

            return fileData;
        }

        public static async Task<FileDataTransformation> UploadFiles(IFormFile files, string filePath = @".\TempFilesUploaded\")
        {

            FileDataTransformation fileData = new FileDataTransformation();

            if (files == null) throw new Exception("No any files for upload.");
            if (string.IsNullOrEmpty(filePath)) throw new Exception("String file path must be declare before upload.");

            //long size = files.Sum(f => f.Length);

            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);

            var filename = string.Empty;


            filename = Guid.NewGuid().ToString() + Path.GetExtension(files.FileName);
            using (var stream = new FileStream(filePath + filename, FileMode.Create)) {

                await files.CopyToAsync(stream);

                fileData = new FileDataTransformation()
                {
                    FILE_NAME = filename,
                    FILE_PATH = filePath + filename,
                    FILE_SIZE = files.Length
                };
                        
            }            

            return fileData;
        }

    }
}
