﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Excel
{
    public class ExcelCellSetting
    {
        public ExcelCellSetting()
        {
        }

        public ExcelCellSetting(string source, string header, string format = null, string customHeader = null)
        {
            this.Source = source;
            this.Header = header;
            this.CellFormat = format;
            this.CustomHeader = customHeader;
        }
        public string Header { get; set; }
        public string Source { get; set; }
        public string CustomHeader { get; set; }
        public string CellFormat { get; set; }
    }

    public class ExcelColumnDisplay
    {
        IEnumerable<ExcelCellSetting> columnSetting = new List<ExcelCellSetting>();

        public ExcelColumnDisplay(IEnumerable<ExcelCellSetting> columnSetting)
        {
            this.columnSetting = columnSetting;
        }

        public string getHeader(string Source, bool isCustom = false)
        {
            if (columnSetting.ToList().Exists(obj => obj.Source == Source))
            {
                if (string.IsNullOrEmpty(columnSetting.ToList().FirstOrDefault(obj => obj.Source == Source).CustomHeader) && isCustom)
                {
                    return columnSetting.FirstOrDefault(obj => obj.Source == Source).Header + " " +
                            columnSetting.FirstOrDefault(obj => obj.Source == Source).CustomHeader;
                }
                else
                {
                    return columnSetting.FirstOrDefault(obj => obj.Source == Source).Header;
                }
            }
            else { return null; }
        }
    }
}
