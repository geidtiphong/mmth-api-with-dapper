﻿using Framework.DataTransformations;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Services.Common.Excel
{
    public class ExcelCriteriaDataTransformation
    {
        public string FILE_PATH { get; set; }
        public int PAGE_SIZE { get; set; }
        public int CURRENT_PAGE_INDEX { get; set; }
        public IEnumerable<ExcelCellSetting> COLUMN_SETTING { get; set; }

        public string PROCESS_ID { get; set; }
        public string PROGRAM_CODE { get; set; }
        public int INTERFACE_NO { get; set; }
        public string FILE_NAME { get; set; }
        public string UPLOAD_BY { get; set; }
        public string DIAMETER { get; set; }

        public bool AUTO_DELETE_FILE { get; set; }

        public ExcelCriteriaDataTransformation() {
            DIAMETER = "|";
            AUTO_DELETE_FILE = true;
        }
    }
}
