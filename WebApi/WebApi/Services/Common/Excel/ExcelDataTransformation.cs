﻿using Framework.DataTransformations;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Services.Common.Excel
{
    public class ExcelDataTransformation 
    {
        public string PROCESS_ID { get; set; }
        public string PROGRAM_CODE { get; set; }
        public int INTERFACE_NO { get; set; }
        public string FILE_NAME { get; set; }
        public string UPLOAD_BY { get; set; }
        public DateTime UPLOAD_DATE { get; set; }
        public string FILE_STATUS { get; set; }
        public int DATA_LINE { get; set; }
        public string DATA_ROW_VALUE { get; set; }
        public string ERROR_MESSAGE { get; set; }

    }
}
