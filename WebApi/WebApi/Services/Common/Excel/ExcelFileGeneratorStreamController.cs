﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Framework.Models;
using WebApi.Services.Common.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace WebApi.Services.Common.Excel
{
    public class ExcelFileGeneratorStreamController : ControllerBase
    {
        public IActionResult FileExportStream<T>(IEnumerable<T> entities, IEnumerable<ExcelCellSetting> cellSetting, string strFileName = "Export")
        {
            var fileKey = $@"{strFileName}_{DateTime.Now.ToString("yyyyMMddHHmmss")}";
            var fileName = $@"{fileKey}.xlsx";

            DataTable dataTable = new DataTable();
            dataTable = CreateDataTable<T>(entities, cellSetting);

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");                
               
                int count = 1;
                foreach (var temp in cellSetting)
                {
                    string header = temp.Header;
                    string formatdata = temp.CellFormat;
                    /*
                    var allCells = worksheet.Cells[1, count];
                    var cellFont = allCells.Style.Font;
                    cellFont.Bold = true;
                    */

                    worksheet.Cells[1, count].Value = header;
                    if (formatdata != null)
                    {
                        worksheet.Column(count).Style.Numberformat.Format = formatdata;
                    }                    
                    worksheet.Column(count).AutoFit();                    
                    count++;
                }
                worksheet.Cells["A1"].LoadFromDataTable(dataTable, PrintHeaders: true);

                count = 1;
                foreach (var temp in cellSetting)
                {
                    string header = temp.Header;
                    worksheet.Cells[1, count].Value = header;
                    count++;
                }

                /*  
                -- AUTO FILLTER
                worksheet.Cells[worksheet.Dimension.Address].AutoFilter = true;
                */
                string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File(package.GetAsByteArray(), XlsxContentType, fileName);
            }
        }

        public IActionResult TemplateExportStream(IEnumerable<ExcelCellSetting> cellSetting, string strFileName = "Template")
        {
            var fileKey = $@"{strFileName}_{DateTime.Now.ToString("yyyyMMddHHmmss")}";
            var fileName = $@"{fileKey}.xlsx";

            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add("Sheet1");
                int count = 1;
                foreach (var temp in cellSetting)
                {
                    string header = temp.Header;
                    string formatdata = temp.CellFormat;

                    worksheet.Cells[1, count].Value = header;
                    if (formatdata != null)
                    {
                        worksheet.Column(count).Style.Numberformat.Format = formatdata;
                    }
                    worksheet.Column(count).AutoFit();
                    count++;
                }
                string XlsxContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return File(package.GetAsByteArray(), XlsxContentType, fileName);
            }
        }

        public DataTable CreateDataTable<T>(IEnumerable<T> entities, IEnumerable<ExcelCellSetting> cellSetting)
        {
            Dictionary<string, int> mpaName = new Dictionary<string, int>();
            Dictionary<string, int> mpaName_cell = new Dictionary<string, int>();
            var convertProperties = TypeDescriptor.GetProperties(typeof(T)).Cast<PropertyDescriptor>();

            DataTable dataTable = new DataTable();
            foreach (var cell in cellSetting)
            {
                string source = cell.Source;

                mpaName[source] = (mpaName.ContainsKey(source)) ? mpaName[source] + 1 : 1;

                var convertProperty = convertProperties.FirstOrDefault(prop => prop.Name == source);
                source = source + mpaName[source];

                if (convertProperty != null)
                {
                    var tmp_typenew = convertProperty.PropertyType;
                    if (convertProperty.PropertyType.IsGenericType && convertProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                        tmp_typenew = convertProperty.PropertyType.GenericTypeArguments[0];

                    Type type = tmp_typenew;
                    dataTable.Columns.Add(source, type).DefaultValue = DBNull.Value;
                }
                else
                {
                    dataTable.Columns.Add(source, typeof(string)).DefaultValue = DBNull.Value;
                }
            }

            DataRow row;
            foreach (T entity in entities)
            {
                mpaName_cell = new Dictionary<string, int>();

                row = dataTable.NewRow();
                foreach (var cell in cellSetting)
                {
                    var temp = entity.GetType().GetProperty(cell.Source);
                    if (temp != null)
                    {
                        string source = cell.Source;

                        mpaName_cell[source] = (mpaName_cell.ContainsKey(source)) ? mpaName_cell[source] + 1 : 1;

                        source = source + mpaName_cell[source];

                        var value = entity.GetType().GetProperty(cell.Source).GetValue(entity);
                        if (value != null) row[source] = value;
                    }
                }
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }

        private static ExcelColumnDisplay columnDisplay = null;

        public static ResponseModel<IEnumerable<T>> ReadDataFromExcel<T, T2>(ExcelCriteriaDataTransformation excelCriteria)
        {
            var t = typeof(T2);
            var convertProperties = TypeDescriptor.GetProperties(typeof(T2)).Cast<PropertyDescriptor>();
            var propertyInfos = TypeDescriptor.GetProperties(typeof(T)).Cast<PropertyDescriptor>();

            /* ต้องเพิ่ม result ที่ model */
            var propertyInfo_result = propertyInfos.FirstOrDefault(prop => prop.Name == "RESULT");
            if (propertyInfo_result == null)
            {
                throw new Exception("Please add property name 'RESULT' in model.");
            }
            T tempData = Activator.CreateInstance<T>();

            List<T> entities = new List<T>();
            ResponseModel<IEnumerable<T>> excelReaderResponse = new ResponseModel<IEnumerable<T>>();

            if (excelCriteria.COLUMN_SETTING != null)
            {
                columnDisplay = new ExcelColumnDisplay(excelCriteria.COLUMN_SETTING);
            }
            byte[] bin = System.IO.File.ReadAllBytes(excelCriteria.FILE_PATH);

            using (MemoryStream stream = new MemoryStream(bin))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                /* loop first worksheets */
                ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();

                int rows = worksheet.Dimension.Rows;
                int columns = worksheet.Dimension.Columns;
                Boolean checkerror = false;

                excelReaderResponse.TotalRows = rows;

                for (var j = 2; j <= rows; j++)
                {
                    tempData = Activator.CreateInstance<T>();

                    var i = 1;
                    string ERROR_MESSAGE = "";
                    Boolean checknull = true;
                    foreach (var propertyInfo in propertyInfos)
                    {
                        if (propertyInfo.Name == "RESULT")
                        {
                            continue;
                        }
                        IEnumerable<ExcelCellSetting> tempExcelCell =  excelCriteria.COLUMN_SETTING.Select(x => x).Where(x => x.Source == propertyInfo.Name);
                        if (tempExcelCell == null || tempExcelCell.Count() == 0) continue;

                        string COLUMN_NAME = tempExcelCell.First().Header;

                        var cell = worksheet.Cells[j, i];

                        var value_text = cell.Text.Trim();
                        var value_object = cell.Value;

                        if (value_object != null && value_object.ToString().Trim() != "")
                        {
                            checknull = false;
                        }
                        var convertProperty = convertProperties.FirstOrDefault(prop => prop.Name == propertyInfo.Name);
                        if (convertProperty != null)
                        {
                            /* check Required attribute */
                            var pi = t.GetProperty(convertProperty.Name);
                            Boolean hasIsIdentity = Attribute.IsDefined(pi, typeof(RequiredAttribute));

                            var tmp_typenew = convertProperty.PropertyType;
                            if (convertProperty.PropertyType.IsGenericType && convertProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                                tmp_typenew = convertProperty.PropertyType.GenericTypeArguments[0];

                            /* check convert value */
                            if (typeof(string) != tmp_typenew)
                            {
                                if (hasIsIdentity && value_text == "")
                                {
                                    if (ERROR_MESSAGE == "") ERROR_MESSAGE = "Column name '" + COLUMN_NAME + "' is require.";
                                    propertyInfo.SetValue(tempData, null);
                                }
                                else if (value_text == "")
                                {
                                    propertyInfo.SetValue(tempData, null);
                                }
                                else
                                {
                                    if (tmp_typenew == typeof(Int32))
                                    {
                                        bool success = Int32.TryParse(value_text, out var temp);
                                        if (!success) if (ERROR_MESSAGE == "") ERROR_MESSAGE = "Column name '" + COLUMN_NAME + " cannot convert value '" + value_text + "' to type integer."; ;
                                    }
                                    else if (tmp_typenew == typeof(decimal))
                                    {
                                        bool success = decimal.TryParse(value_text, out var temp);
                                        if (!success) if (ERROR_MESSAGE == "") ERROR_MESSAGE = "Column name '" + COLUMN_NAME + " cannot convert value '" + value_text + "' to type decimal."; ;
                                    }
                                    else if (tmp_typenew == typeof(DateTime))
                                    {
                                        try
                                        {
                                            var unboxedVal = (double)value_object;
                                            DateTime result = DateTime.FromOADate(unboxedVal);
                                        }
                                        catch (Exception)
                                        {
                                            if (ERROR_MESSAGE == "") ERROR_MESSAGE = "Column name '" + COLUMN_NAME + " cannot convert value '" + value_text + "' to type datetime.";
                                        }
                                    }
                                    propertyInfo.SetValue(tempData, value_text);
                                }
                            }
                            else
                            {
                                /* if required field */
                                if (hasIsIdentity) if (value_text == "") if (ERROR_MESSAGE == "") ERROR_MESSAGE = "Column name '" + COLUMN_NAME + "' is require.";
                                propertyInfo.SetValue(tempData, value_text);

                                /* ceck string length */
                                Boolean hasStringLength = Attribute.IsDefined(pi, typeof(StringLengthAttribute));
                                if (hasStringLength)
                                {
                                    StringLengthAttribute strLenAttr = (StringLengthAttribute)Attribute.GetCustomAttribute(pi, typeof(StringLengthAttribute), true);
                                    if (value_text.Length > strLenAttr.MaximumLength)
                                    {
                                        if (ERROR_MESSAGE == "") ERROR_MESSAGE = "Column name '" + COLUMN_NAME + "' length of string is over " + strLenAttr.MaximumLength + " characters";
                                    }
                                }
                            }
                        }
                        i++;
                    }
                    if (!checknull)
                    {
                        if (ERROR_MESSAGE != "")
                        {
                            checkerror = true;
                        }
                        propertyInfo_result.SetValue(tempData, ERROR_MESSAGE);
                        entities.Add(tempData);
                    }
                }
                if (checkerror)
                {
                    excelReaderResponse.ReturnStatus = false;
                    excelReaderResponse.ReturnMessage.Add("Cannot upload please see result as below.");
                }
                if (excelCriteria.AUTO_DELETE_FILE)
                {
                    if (System.IO.File.Exists(excelCriteria.FILE_PATH)) System.IO.File.Delete(excelCriteria.FILE_PATH);
                }
                excelReaderResponse.Entity = entities;
                return excelReaderResponse;
            }
        }
    }
}