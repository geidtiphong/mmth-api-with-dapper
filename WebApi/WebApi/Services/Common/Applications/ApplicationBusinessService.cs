﻿using Framework.Models;
using Framework.Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Applications {
    public class ApplicationBusinessService : BusinessServiceBase {

        public ApplicationBusinessService(IOptions<ConnectionStrings> config) : base(config) {
        }

    }
}
