﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.FileExport {
    public class FileExportDataTransformation {

        public string FILE_KEY { get; set; }
        public string FILE_PATH { get; set; }
        public string FILE_NAME { get; set; }

    }
}
