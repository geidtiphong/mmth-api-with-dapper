﻿using Dapper;
using Framework.Models;
using Framework.Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.FileExport
{
    public class FileExportBusinessService : BusinessServiceBase
    {

        public FileExportBusinessService(IOptions<ConnectionStrings> config) : base(config) {
        }

        public async Task<FileExportDataTransformation> Insert(FileExportDataTransformation dto) {

            using (SqlConnection conn = GetOpenConnection()) {
                string sql = @"INSERT INTO [dbo].[IM_TBL_EXP_FILES] (FILE_KEY, FILE_PATH, FILE_NAME)
                    VALUES ('{0}', '{1}', '{2}')";
                await conn.ExecuteAsync(string.Format(sql, dto.FILE_KEY, dto.FILE_PATH, dto.FILE_NAME));
            }

            if (dto != null) {
                dto.FILE_PATH = null;
            }

            return dto;
        }

        public async Task<FileExportDataTransformation> Select(FileExportCriteriaDataTransformation dto) {

            FileExportDataTransformation entity;
            using (SqlConnection conn = GetOpenConnection()) {
                string sql = "SELECT * FROM [dbo].[IM_TBL_EXP_FILES] WHERE FILE_KEY = '{0}'";
                entity = await conn.QueryFirstOrDefaultAsync<FileExportDataTransformation>(
                    string.Format(sql, dto.FILE_KEY));
            }

            return entity;
        }
    }
}
