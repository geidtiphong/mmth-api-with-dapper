﻿namespace WebApi.Services.Common.Login
{
    public class RoleDataTransformation
    {
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
    }
}
