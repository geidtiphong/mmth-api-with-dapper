﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApi.Services.Common.Login
{
    public class LoginDataTransformation
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public string RoleCode { get; set; }
        public string Password { get; set; }
        public string PasswordConfirmation { get; set; }
        public string Token { get; set; }
        public bool IsAuthenicated { get; set; }
    }
}
