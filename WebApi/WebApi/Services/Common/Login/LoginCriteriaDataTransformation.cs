﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Login
{
    public class LoginCriteriaDataTransformation
    {
        public List<Role> Roles { get; set; }

        public LoginCriteriaDataTransformation()
        {
            Roles = new List<Role>();
        }
    }

    public class Role
    {
        public string RoleCode { get; set; }
        public string RoleDescription { get; set; }
    }
}
