﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Login
{
    public class MenuDataTransformation
    {
        public int GroupId { get; set; }
        public int ParentGroupId { get; set; }
        public string GroupName { get; set; }
        public int GroupSequence { get; set; }
        public int ProgramId { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramName { get; set; }
        public List<MenuDataTransformation> Children { get; set; }

        public MenuDataTransformation() {
            Children = new List<MenuDataTransformation>();
        }
    }
}
