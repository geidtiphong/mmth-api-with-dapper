﻿using Dapper;
using Framework.Models;
using Framework.Services;
using Framework.Utilities;
using WebApi.Services.Entities;
using System;
using static System.Data.CommandType;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Data;
using System.Collections.Generic;
using WebApi.Services.Common.Select;
using Microsoft.Extensions.Options;
using System.Linq;
using Framework.Helpers;

namespace WebApi.Services.Common.Login {
    public class AuthorizationBusinessService : BusinessServiceBase {

        public AuthorizationBusinessService(IOptions<ConnectionStrings> config) : base(config) {
            _connectionString = config.Value.PrimaryDatabaseConnectionString;
        }

        public async Task<TreeNode> GetMenusByOrganizeRole(MenuRequestDataTransformation menuRequest) {
            string sql = "GetMenuByOrganizeRole";

            IEnumerable<MenuDataTransformation> results = await GetItems<MenuDataTransformation>(StoredProcedure, sql, new { ORGANIZE_ID = menuRequest.OrganizeId, ROLE_CODE = menuRequest.RoleCode });

            MenuDataTransformation menuRoot = results.AsList().FirstOrDefault(x => x.GroupId == x.ParentGroupId);
            TreeNode treeNodeRoot = null;
            if (menuRoot != null) {
                treeNodeRoot = new TreeNode() { Id = menuRoot.GroupId, ParentId = menuRoot.ParentGroupId, Name = menuRoot.GroupName };
                buildChildren(results.AsList(), treeNodeRoot);
            }

            return treeNodeRoot;
        }

        private void buildChildren(List<MenuDataTransformation> menuList, TreeNode node) {
            menuList.Where(x => x.ParentGroupId != x.GroupId && x.ParentGroupId == node.Id).ToList().ForEach(delegate (MenuDataTransformation item) {
                if (node.Children.FirstOrDefault(i => i.Id == item.GroupId) == null) {
                    TreeNode treeNode = new TreeNode() { Id = item.GroupId, ParentId = item.ParentGroupId, Name = item.GroupName };
                    node.Children.Add(treeNode);

                    menuList.Where(y => y.GroupId == item.GroupId && y.ParentGroupId == item.ParentGroupId).ToList().ForEach(delegate (MenuDataTransformation child)
                    {
                        TreeNodeItem noteItem = new TreeNodeItem() { ProgramId = child.ProgramId, ProgramCode = child.ProgramCode, ProgramName = child.ProgramName };
                        treeNode.TreeNodeItems.Add(noteItem);
                    });

                    buildChildren(menuList, treeNode);
                }
            });
        }

        public async Task<ResponseModel<IEnumerable<RoleDataTransformation>>> GetRoles() {
            ResponseModel<IEnumerable<RoleDataTransformation>> returnResponse = new ResponseModel<IEnumerable<RoleDataTransformation>>();

            List<RoleDataTransformation> dataTransformation = new List<RoleDataTransformation>();

            try {
                string sql = "GetRoles";
                IEnumerable<Role> entities = await GetItems<Role>(StoredProcedure, sql);
                entities.AsList().ForEach(delegate (Role entity) {
                    dataTransformation.Add(new RoleDataTransformation() { RoleCode = entity.RoleCode, RoleName = entity.RoleDescription });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectDataTransformation>>> GetOrganizesByRole(string roleCodeParameter) {
            ResponseModel<IEnumerable<SelectDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectDataTransformation>>();

            List<SelectDataTransformation> dataTransformation = new List<SelectDataTransformation>();
            try {
                string sql = "GetOrganizeByRole";
                IEnumerable<Organize> entities = await GetItems<Organize>(StoredProcedure, sql, new { ROLE_CODE = roleCodeParameter });

                entities.AsList().ForEach(delegate (Organize entity) {
                    dataTransformation.Add(new SelectDataTransformation() {
                        OrganizeId = entity.OrganizeId,
                        OrganizeName = entity.OrganizeName
                    });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }


        public async Task<ResponseModel<LoginDataTransformation>> Login(LoginDataTransformation dataTransformation) {
            ResponseModel<LoginDataTransformation> returnResponse = new ResponseModel<LoginDataTransformation>();

            MDFN_TBL_USER user = new MDFN_TBL_USER();

            try {
                using (SqlConnection conn = new SqlConnection(this._connectionString)) {
                    conn.Open();

                    DynamicParameters parameters = new DynamicParameters();
                    parameters.Add("@USER_NAME", dataTransformation.UserName.ToLower());
                    //parameters.Add("@Password", dataTransformation.Password);
                    string sql = "SELECT * FROM MDFN_TBL_USER WHERE USER_NAME=@USER_NAME";
                    user = await SqlMapper.QuerySingleOrDefaultAsync<MDFN_TBL_USER>(conn, sql, parameters, commandType: Text);
                }

                if (user == null) {
                    returnResponse.ReturnStatus = false;
                    returnResponse.ReturnMessage.Add("Login incorrect.");
                    return returnResponse;
                }

                string hashedPassword = Hasher.GenerateHash(dataTransformation.Password + user.PASSWORDSALT);
                if (user.PASSWORD != hashedPassword) {
                    returnResponse.ReturnStatus = false;
                    returnResponse.ReturnMessage.Add("Login incorrect.");
                    return returnResponse;
                }

                dataTransformation.UserId = user.MDFN_TBL_USER_ID;
                dataTransformation.FirstName = user.FIRST_NAME;
                dataTransformation.LastName = user.LAST_NAME;
                dataTransformation.UserName = user.USER_NAME;
                dataTransformation.Password = string.Empty;

                returnResponse.Entity = dataTransformation;

                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }



            return returnResponse;

        }

        public async Task<ResponseModel<LoginDataTransformation>> Register(LoginDataTransformation dataTransformation) {

            ResponseModel<LoginDataTransformation> returnResponse = new ResponseModel<LoginDataTransformation>();

            MDFN_TBL_USER user = new MDFN_TBL_USER();

            dataTransformation.UserName = dataTransformation.UserName.ToLower();

            try {
                using (SqlConnection conn = new SqlConnection(_connectionString)) {
                    conn.Open();

                    user.USER_NAME = dataTransformation.UserName;
                    user.FIRST_NAME = dataTransformation.FirstName;
                    user.LAST_NAME = dataTransformation.LastName;
                    user.USER_NAME = dataTransformation.UserName;
                    user.CREATED_BY = "ADMIN";

                    string salt = Hasher.GetSalt();
                    string hashedPassword = Hasher.GenerateHash(dataTransformation.Password + salt);

                    user.PASSWORD = hashedPassword;
                    user.PASSWORDSALT = salt;

                    string sql = "CreateUser";
                    await Execute(StoredProcedure, sql, new { user.USER_NAME, user.PASSWORD, user.PASSWORDSALT, user.FIRST_NAME, user.LAST_NAME, user.CREATED_BY });

                    sql = "GetUserByUserName";
                    IEnumerable<MDFN_TBL_USER> entities = await GetItems<MDFN_TBL_USER>(StoredProcedure, sql, new { user.USER_NAME });
                    if (entities != null && entities.Count() > 0) {
                        dataTransformation.UserId = entities.AsList().First().MDFN_TBL_USER_ID;
                        dataTransformation.Password = string.Empty;
                        dataTransformation.PasswordConfirmation = string.Empty;
                    }


                    returnResponse.Entity = dataTransformation;
                    returnResponse.ReturnStatus = true;
                }

            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }
    }
}
