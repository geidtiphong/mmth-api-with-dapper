﻿namespace WebApi.Services.Common.Login
{
    public class MenuRequestDataTransformation
    {
        public int OrganizeId { get; set; }
        public string RoleCode { get; set; }
    }
}
