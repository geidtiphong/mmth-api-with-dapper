﻿using Framework.Models;
using Microsoft.Extensions.Options;
using System;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using System.Data;
using Framework.Utilities;

namespace WebApi.Services.Common
{
    public class InfoService
    {        
        protected string _connectionString;
        protected ApplicationInfo _appInfo;

        public InfoService(IOptions<ConnectionStrings> connectionStrings, IOptions<ApplicationInfo> applicationInfo)
        {
            _connectionString = connectionStrings.Value.PrimaryDatabaseConnectionString;
            _appInfo = applicationInfo.Value;
        }

        public ApplicationInfo GetApplicationInfo()
        {
            return _appInfo;
        }

        public string GetDatabaseInfo()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(_connectionString);
            return string.Format("DataSource={0}, InitialCatalog={1}, DatabaseTime={2}",builder.DataSource,builder.InitialCatalog, GetDatabaseTime());
        }

        private string GetDatabaseTime()
        {
            string result = "";

            try
            {
                using (IDbConnection db = new SqlConnection(_connectionString))
                {
                    db.Open();
                    result = db.Query<string>("select getdate()").Single();
                }
            }
            catch(Exception ex)
            {
                result = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
            }                            

            return result;
        }
    }
}
