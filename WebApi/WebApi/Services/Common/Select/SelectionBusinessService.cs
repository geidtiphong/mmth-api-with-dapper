﻿using Dapper;
using Framework.Models;
using Framework.Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using System.Data.SqlClient;

namespace WebApi.Services.Common.Select {
    public class SelectionBusinessService : BusinessServiceBase {

        public SelectionBusinessService(IOptions<ConnectionStrings> config) : base(config) {
        }

        public ResponseModel<PagingUpload<T>> PagingExcel<T>(ResponseModel<IEnumerable<T>> excelReaderResponse
            , int PageSize, int CurrentPageIndex, string FILE_PATH)
        {
            ResponseModel<PagingUpload<T>> resultlEntity = new ResponseModel<PagingUpload<T>>();
            SelectPaging<T> tmpCriteria = new SelectPaging<T>();
            tmpCriteria.entityResponse = excelReaderResponse.Entity;
            tmpCriteria.PageSize = PageSize;
            tmpCriteria.CurrentPageIndex = CurrentPageIndex;

            resultlEntity = Paging<T>(tmpCriteria);

            resultlEntity.Entity.FILE_PATH = FILE_PATH;
            return resultlEntity;
        }

        public T2 copydatatomodel<T, T2>(T entity)
        {
            var convertProperties = TypeDescriptor.GetProperties(typeof(T2)).Cast<PropertyDescriptor>();
            var entityProperties = TypeDescriptor.GetProperties(entity).Cast<PropertyDescriptor>();
            T2 convert = Activator.CreateInstance<T2>();
            foreach (var entityProperty in entityProperties)
            {
                var property = entityProperty;
                var convertProperty = convertProperties.FirstOrDefault(prop => prop.Name == property.Name);
                if (convertProperty != null)
                {
                    if (entityProperty.GetValue(entity) != null)
                    {
                        var tmp_typeold = entityProperty.PropertyType;
                        if (entityProperty.PropertyType.IsGenericType && entityProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            tmp_typeold = entityProperty.PropertyType.GenericTypeArguments[0];

                        var tmp_typenew = convertProperty.PropertyType;
                        if (convertProperty.PropertyType.IsGenericType && convertProperty.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                            tmp_typenew = convertProperty.PropertyType.GenericTypeArguments[0];

                        if (tmp_typeold != tmp_typenew)
                            convertProperty.SetValue(convert, Convert.ChangeType(entityProperty.GetValue(entity), tmp_typenew));
                        else
                            convertProperty.SetValue(convert, entityProperty.GetValue(entity));
                    }                        
                    else
                        convertProperty.SetValue(convert, null);                    
                }
            }
            return convert;
        }

        public async Task<ResponseModel<IEnumerable<T>>> GetDataPaging<T>(SelectCriterialDataPaging criteria)
        {
            IEnumerable<T> dataTransformation = new List<T>();
            ResponseModel<IEnumerable<T>> returnResponse = new ResponseModel<IEnumerable<T>>();
            using (SqlConnection conn = GetOpenConnection())
            {
                dataTransformation = await conn.QueryAsync<T>(criteria.Sql);
                returnResponse.Entity = dataTransformation;
            }
            returnResponse.PageSize = criteria.PageSize;
            returnResponse.TotalRows = dataTransformation.Count();

            if (criteria.PageSize == 0 || criteria.CurrentPageIndex == 0)
            {
                return returnResponse;
            }
            if (dataTransformation == null || dataTransformation.Count() == 0)
            {
                return returnResponse;
            }

            int TOTAL_RECORDS = 0, TOTAL_PAGE = 0;
            TOTAL_RECORDS = returnResponse.TotalRows;
            int PAGESIZE = criteria.PageSize;
            TOTAL_PAGE = TOTAL_RECORDS / PAGESIZE;
            if (TOTAL_RECORDS % criteria.PageSize != 0) TOTAL_PAGE++;
            int PAGE = criteria.CurrentPageIndex - 1;

            returnResponse.TotalPages = TOTAL_PAGE;
            returnResponse.Entity = dataTransformation.Skip(PAGE * PAGESIZE).Take(PAGESIZE);
            return returnResponse;
        }

        public ResponseModel<IEnumerable<T>> PagingEntity<T>(SelectPaging<T> criteria)
        {
            ResponseModel<IEnumerable<T>> returnResponse = new ResponseModel<IEnumerable<T>>();
            IEnumerable<T> entity = criteria.entityResponse;

            returnResponse.Entity = new List<T>();
            returnResponse.PageSize = criteria.PageSize;

            int TOTAL_RECORDS = 0, TOTAL_PAGE = 0;
            if (entity == null || entity.Count() == 0)
            {
                return returnResponse;
            }
            returnResponse.TotalRows = entity.Count();
            returnResponse.Entity = entity;
            if (criteria.PageSize == 0 || criteria.CurrentPageIndex == 0)
            {
                return returnResponse;
            }

            TOTAL_RECORDS = returnResponse.TotalRows;
            int PAGESIZE = criteria.PageSize;
            TOTAL_PAGE = TOTAL_RECORDS / PAGESIZE;
            if (TOTAL_RECORDS % criteria.PageSize != 0) TOTAL_PAGE++;
            int PAGE = criteria.CurrentPageIndex - 1;

            returnResponse.TotalPages = TOTAL_PAGE;
            returnResponse.Entity = entity.Skip(PAGE * PAGESIZE).Take(PAGESIZE);
            return returnResponse;
        }

        public ResponseModel<PagingUpload<T>> Paging<T>(SelectPaging<T> criteria)
        {
            PagingUpload<T> pagingUpload = new PagingUpload<T>();
            ResponseModel<PagingUpload<T>> returnResponse = new ResponseModel<PagingUpload<T>>();
            IEnumerable<T> entity = criteria.entityResponse;

            returnResponse.Entity = pagingUpload;
            if (entity == null)
            {
                return returnResponse;
            }
            returnResponse.Entity.EXCEL_DATA = entity;
            returnResponse.TotalRows = entity.Count();

            if (criteria.PageSize == 0 || criteria.CurrentPageIndex == 0)
            {
                return returnResponse;
            }
            int TOTAL_RECORDS = returnResponse.TotalRows;
            int PAGESIZE = criteria.PageSize;
            int TOTAL_PAGE = TOTAL_RECORDS / PAGESIZE;
            if (TOTAL_RECORDS % criteria.PageSize != 0) TOTAL_PAGE++;
            int PAGE = criteria.CurrentPageIndex - 1;
            
            returnResponse.TotalPages = TOTAL_PAGE;
            returnResponse.Entity.EXCEL_DATA = entity.Skip(PAGE * PAGESIZE).Take(PAGESIZE);            
            returnResponse.PageSize = criteria.PageSize;
            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetOrganizes() {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text, "SELECT ORGANIZE_ID SELECTION_KEY, ORGANIZE_NAME SELECTION_VALUE from [dbo].[IM_TBL_MST_ORGANIZE]");
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetItemGroups(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT ITEM_GROUP_ID SELECTION_KEY
                                , CONCAT(ITEM_GROUP_CODE, ' : ', ITEM_GROUP_DES) SELECTION_VALUE 
                            FROM [dbo].[IM_TBL_MST_ITEM_GROUP]
                            WHERE ORGANIZE_ID = {0}";
                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text, string.Format(sql, criteria.ORGANIZE_ID));
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetItemGroupsCode(SelectionCriteriaDataTransformation criteria)
        {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();
            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();
            try
            {
                string sql = @"SELECT DISTINCT ITEM_GROUP_CODE SELECTION_KEY
                                , CONCAT(ITEM_GROUP_CODE, ' : ', ITEM_GROUP_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ITEM_GROUP]
                                WHERE ORGANIZE_ID = {0} ";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text, string.Format(sql, criteria.ORGANIZE_ID));
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation()
                        {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });
                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }
            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetAccounts(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT ACCT_ID SELECTION_KEY
                                    , CONCAT(ACCT_CODE, ' : ', ACCT_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ACCOUNT]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetCodeAccounts(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT ACCT_CODE SELECTION_KEY
                                    , CONCAT(ACCT_CODE, ' : ', ACCT_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ACCOUNT]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetFinancialDimensions(SelectionCriteriaDataTransformation criteria) {
            return await GetFinancialDimensions(criteria, null);
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetFinancialDimensions(SelectionCriteriaDataTransformation criteria, string type) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT DIM_ID SELECTION_KEY, [UNIT_CODE] SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_FC_DIMENSION] WHERE 1=1";

                if (!string.IsNullOrEmpty(type))
                    sql += string.Format(" AND TYPE = '{0}'", type);

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text, sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetCodeFinancialDimensions(SelectionCriteriaDataTransformation criteria) {
            return await GetCodeFinancialDimensions(criteria, null);
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetCodeFinancialDimensions(SelectionCriteriaDataTransformation criteria, string type) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT UNIT_CODE SELECTION_KEY, [UNIT_CODE] SELECTION_VALUE FROM [dbo].[IM_TBL_MST_FC_DIMENSION] WHERE 1=1";

                if (!string.IsNullOrEmpty(type))
                    sql += string.Format(" AND TYPE = '{0}'", type);

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text, sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetItems(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT DISTINCT ITEM_NO SELECTION_KEY
                                    , CONCAT(ITEM_NO, ' : ', ITEM_NAME_ENG) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ITEM] 
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetItemsInRouting(SelectionCriteriaDataTransformation criteria)
        {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try
            { 
                string sql = @"SELECT ITEM_NO SELECTION_KEY
                                    , CONCAT(ITEM_NO, ' : ', ITEM_NAME_ENG) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ITEM] 
                                WHERE ORGANIZE_ID = {0}
                                AND ITEM_ID IN ( SELECT ITEM_ID FROM IM_TBL_MST_ROUTING WHERE ORGANIZE_ID = {0} )";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation()
                        {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetItemsByGroupType(SelectionCriteriaDataTransformation criteria, string GroupType) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT ITEM_ID SELECTION_KEY, CONCAT(ITEM_NO, ' : ', ITEM_NAME_ENG) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ITEM] i 
                                    left join [dbo].[IM_TBL_MST_ITEM_GROUP] ig on i.ITEM_GROUP_ID = ig.ITEM_GROUP_ID
                                WHERE ig.ITEM_GROUP_TYPE = '{0}'
                                    AND i.ORGANIZE_ID = {1}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text,
                    string.Format(sql, GroupType, criteria.ORGANIZE_ID));
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetItemTypes(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT ITEM_TYPE_ID SELECTION_KEY
                                    , CONCAT(ITEM_TYPE_CODE, ' : ', ITEM_TYPE_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ITEM_TYPE]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetWarehouses(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT WHSE_ID SELECTION_KEY
                                    , CONCAT(WHSE_CODE, ' : ', WHSE_NAME) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_WAREHOUSE]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetPaymentTerms(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT PAYMENT_TERM_ID SELECTION_KEY
                                    , CONCAT(PAYMENT_TERM_CODE, ' : ', PAYMENT_TERM_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_PAYMENT_TERM]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetBanks(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT BANK_ID SELECTION_KEY
                                    , CONCAT(BANK_CODE, ' : ', BANK_NAME) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_BANK]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetTaxCodes(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT TAX_CODE_ID SELECTION_KEY
                                    , CONCAT(TAX_CODE, ' : ', TAX_CODE_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_TAX_CODE]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetLocations(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT LOC_ID SELECTION_KEY
                                    , CONCAT(LOC_CODE, ' : ', LOC_NAME) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_LOCATION]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetUnitItems(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT UNIT_ITEM_CODE SELECTION_KEY
                                    , CONCAT(UNIT_ITEM_CODE, ' : ', UNIT_ITEM_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_UNIT_ITEM]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetVendors(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT VENDOR_ID SELECTION_KEY
                                    , CONCAT(VENDOR_CODE, ' : ', VENDOR_NAME_ENG) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_VENDOR]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetCustomers(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT CUSTOMER_ID SELECTION_KEY
                                    , CONCAT(CUSTOMER_CODE, ' : ', CUSTOMER_NAME_ENG) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_CUSTOMER]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetYearGLPeriods(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [YEAR] SELECTION_KEY, [YEAR] SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_GL_PERIOD] 
                                WHERE ORGANIZE_ID = {0}
                                GROUP BY [YEAR]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetPeriodGLPeriods(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [PERIOD] SELECTION_KEY, [PERIOD] SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_GL_PERIOD] 
                                WHERE ORGANIZE_ID = {0}
                                GROUP BY [PERIOD]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetCurrencies(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [CURR_CODE] SELECTION_KEY
                                    , CONCAT(CURR_CODE, ' : ', CURR_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_CURRENCY]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetWorkCenters(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [WC_NO] SELECTION_KEY
                                    , CONCAT(WC_NO, ' : ', WC_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_WORK_CENTER]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetRoutes(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [ROUTING_ID] SELECTION_KEY
                                    , CONCAT(OPN_NO, ' : ', OPN_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_ROUTING]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetReasons(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [REASON_ID] SELECTION_KEY
                                    , CONCAT(REASON_CODE, ' : ', REASON_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_REASON]
                                WHERE ORGANIZE_ID = {0}";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetTransType(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [TRANS_TYPE_CODE] SELECTION_KEY
                                    , CONCAT([TRANS_TYPE_CODE], ' : ', [TRANS_TYPE_DES]) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_TRANS_TYPE]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetRefType(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT [TRANS_REF_CODE] SELECTION_KEY
                                    , CONCAT([TRANS_REF_CODE], ' : ', [TRANS_REF_DES]) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_REF_TYPE]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetJournals(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT JOURNAL_ID SELECTION_KEY
                                    , CONCAT(JOURNAL_CODE, ' : ', JOURNAL_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_JOURNAL]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetCodeJournals(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT JOURNAL_CODE SELECTION_KEY
                                    , CONCAT(JOURNAL_CODE, ' : ', JOURNAL_DES) SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_JOURNAL]";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetMonthPeriod(SelectionCriteriaDataTransformation criteria) {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try {
                string sql = @"SELECT CONVERT(varchar(4), YEAR) + '-' + PERIOD  SELECTION_KEY
                                    , CONVERT(varchar(4), YEAR) + '-' + PERIOD  SELECTION_VALUE 
                                FROM [dbo].[IM_TBL_MST_GL_PERIOD]
                                WHERE ORGANIZE_ID = {0}
                                GROUP BY CONVERT(varchar(4), YEAR) + '-' + PERIOD";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation() {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            } catch (Exception ex) {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetOperation(SelectionCriteriaDataTransformation criteria)
        {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try
            {
                string sql = @" SELECT [LOOKUP_CODE] SELECTION_KEY, [LOOKUP_CODE] SELECTION_VALUE 
	                            FROM [dbo].[MDFN_TBL_LOOKUP]
	                            WHERE [SYSTEM_NAME] = '{0}' AND [LOOKUP_TYPE]='OPERATION_NO'";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation()
                        {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetBackflush(SelectionCriteriaDataTransformation criteria)
        {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try
            {
                string sql = @" SELECT [LOOKUP_CODE] SELECTION_KEY, [LOOKUP_VALUE] SELECTION_VALUE 
	                            FROM [dbo].[MDFN_TBL_LOOKUP]
	                            WHERE [SYSTEM_NAME] = '{0}' AND [LOOKUP_TYPE]='BACKFLUSH_FLAG'";

                IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                    , string.Format(sql, criteria.ORGANIZE_ID));

                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation()
                        {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });

                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetConst(SelectionCriteriaStaticDataTransformation criteria)
        {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();

            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();


            string sql = @"SELECT LOOKUP_CODE SELECTION_KEY
                                    , LOOKUP_VALUE SELECTION_VALUE 
                                FROM [dbo].[MDFN_TBL_LOOKUP]
                                WHERE LOOKUP_TYPE = '{0}'
                                ORDER BY LOOKUP_ORDER";

            IEnumerable<Selection> entities = await GetItems<Selection>(CommandType.Text
                , string.Format(sql, criteria.CONST_TYPE));

            entities.AsList().ForEach(delegate (Selection entity) {
                dataTransformation.Add(
                    new SelectionDataTransformation()
                    {
                        KEY = entity.SELECTION_KEY,
                        VALUE = entity.SELECTION_VALUE
                    });
            });

            returnResponse.Entity = dataTransformation;
            returnResponse.ReturnStatus = true;

            return returnResponse;
        }

        public async Task<ResponseModel<IEnumerable<SelectionDataTransformation>>> GetSelectByOrganizeID(string sql, SelectionCriteriaDataTransformation criteria)
        {
            ResponseModel<IEnumerable<SelectionDataTransformation>> returnResponse = new ResponseModel<IEnumerable<SelectionDataTransformation>>();
            List<SelectionDataTransformation> dataTransformation = new List<SelectionDataTransformation>();

            try
            {
                sql = (criteria == null) ? sql : string.Format(sql, criteria.ORGANIZE_ID);
                IEnumerable<Selection> entities = await SqlMapper.QueryAsync<Selection>(GetOpenConnection(), sql);
                entities.AsList().ForEach(delegate (Selection entity) {
                    dataTransformation.Add(
                        new SelectionDataTransformation()
                        {
                            KEY = entity.SELECTION_KEY,
                            VALUE = entity.SELECTION_VALUE
                        });
                });
                returnResponse.Entity = dataTransformation;
                returnResponse.ReturnStatus = true;
            }
            catch (Exception ex)
            {
                returnResponse.ReturnStatus = false;
                returnResponse.ReturnMessage.Add(ex.Message);
            }
            return returnResponse;
        }
    }
}
