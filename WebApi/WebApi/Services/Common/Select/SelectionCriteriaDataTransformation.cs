﻿using Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Select
{
    public class SelectionCriteriaDataTransformation
    {
        public int ORGANIZE_ID { get; set; }
    }

    public class SelectCriterialDataPaging
    {
        public string Sql { get; set; }
        public int CurrentPageIndex { get; set; }
        public int PageSize { get; set; }
    }

    public class SelectPaging<T>
    {
        public IEnumerable<T> entityResponse;
        public int CurrentPageIndex { get; set; }
        public int PageSize { get; set; }

        public SelectPaging()
        {
            entityResponse = new List<T>();
            CurrentPageIndex = 0;
            PageSize = 0;
        }
    }

    public class PagingUpload<T>
    {
        public IEnumerable<T> EXCEL_DATA { get; set; }
        public string FILE_PATH { get; set; }

        public PagingUpload()
        {
            EXCEL_DATA = new List<T>();
            FILE_PATH = "";
        }
    }

    public class Selection {

        public string SELECTION_KEY { get; set; }
        public string SELECTION_VALUE { get; set; }

    }

    public class SelectionCriteriaStaticDataTransformation
    {
        public string CONST_TYPE { get; set; }
    }
    
}
