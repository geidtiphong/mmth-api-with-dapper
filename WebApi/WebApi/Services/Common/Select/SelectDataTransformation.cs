﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Select
{
    public class SelectDataTransformation
    {
        public int OrganizeId { get; set; }
        public string OrganizeName { get; set; }
    }
}
