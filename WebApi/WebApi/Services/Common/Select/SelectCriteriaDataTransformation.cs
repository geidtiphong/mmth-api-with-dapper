﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services.Common.Select
{
    public class SelectCriteriaDataTransformation
    {
        public List<Organize> Organizes { get; set; }

        public SelectCriteriaDataTransformation()
        {
            Organizes = new List<Organize>();
        }
    }

    public class Organize
    {
        public int OrganizeId { get; set; }
        public string OrganizeName { get; set; }
    }
}
